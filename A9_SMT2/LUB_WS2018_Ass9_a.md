# coding=utf-8
from z3 import *
print "LUB | Assignment 9 | Task 61 | Literal a:"


#-------------------------------------------------------------------------------
# Define Z3 variables:
n = Int('n')
x = Int('x')
b = Int('b')
s = Solver()
s.add(n == 4)
# s.add(x == 36)


#-------------------------------------------------------------------------------
# Define python variables:
n = 4
m = n + 4
board = []


#-------------------------------------------------------------------------------
# Initialize chessboard:
for i in range(m*m):
    board.append(i)


#-------------------------------------------------------------------------------
# Compute a set of all valide positions:
valid_set = []
for position in board:
    current_row = position/m
    if ((position > m*2 - 1) and
        (position < m*m - 2*m - 1) and
        (position > current_row*m + 1) and
        (position < current_row*m + m - 2)):
        valid_set.append(position)


#-------------------------------------------------------------------------------
# Compute a set of all invalid positions:
invalid_set = []
for position in board:
    current_row = position/m
    if ((position < m*2 - 1) or
        (position > m*m - 2*m - 1) or
        (position < current_row*m + 2) or
        (position > current_row*m + m - 3)):
        invalid_set.append(position)


#-------------------------------------------------------------------------------
# Constraint: b can only be a number of the valid_set
s.add(Or(b == 18, b == 19, b == 20, b == 21,
         b == 26, b == 27, b == 28, b == 29,
         b == 34, b == 35, b == 36, b == 37,
         b == 42, b == 43, b == 44, b == 45))

s.add(Or(x == 18, x == 19, x == 20, x == 21,
         x == 26, x == 27, x == 28, x == 29,
         x == 34, x == 35, x == 36, x == 37,
         x == 42, x == 43, x == 44, x == 45))


#-------------------------------------------------------------------------------
# Constraint: Knight movement rules:
#   x ... current_position
#   b ... possible next_position
#   n ... side_length of inner_square
s.add(Or((x - 2*(n + 4) - 1 == b), (x - 2*(n + 4) + 1 == b),
         (x -   (n + 4) - 2 == b), (x -   (n + 4) + 2 == b),
         (x +   (n + 4) - 2 == b), (x +   (n + 4) + 2 == b),
         (x + 2*(n + 4) - 1 == b), (x + 2*(n + 4) + 1 == b)))


#-------------------------------------------------------------------------------
# Task_a:   Model the constraints that a move must be on the 4 × 4
#           board and not in the forbidden region.
#-------------------------------------------------------------------------------
while (s.check() != unsat):
    if (s.check() == sat):
        print "x:", s.model()[x], "b:", s.model()[b]
        s.add(x != s.model()[x])
    else:
        print "UNSAT"
    pass
