# Logic and Computability WS18
## Assignment 7
For each of the following sequents, either provide a natural deduction proof,
or a counter-example that proves the sequent invalid.
For proofs, clearly indicate which rule, and what assumptions/premises/
intermediate results you are using in each step.
Also clearly indicate the scope of any boxes you use.
For counterexamples, give a complete model.
Show that the model satisfies the premise(s) of the sequent in question,
but does not satisfy the respective conclusion.

+ Quantifier Equivalence
  - `¬∀xΦ ≡ ∃x¬Φ`
  - `¬∃xΦ ≡ ∀x¬Φ`
  - Quantifier Equivalences were not allowed for this exercise sheet.

### Task_41 [1 Point]
  + Sequence: `∀xQ(x) ⊢ ∃xQ(x)`
  ```
  1. ∀xQ(x)       | premise
  2. Q(t)         | ∀e 1
  n. ∃xQ(x)       | ∃i 2
  ```

### Task_42 [1 Point]
  + Sequence: `¬∀xQ(x) ⊢ ∃xQ(x)`
  + Model M:
    - Domain-set: A = {a,b}
    - Predicate: Q^M = {}
  + Counter example:
    ```
    ¬∀xQ(x) ⊢ ∃xQ(x)
    ¬∀x⊥ ⊢ ∃x⊥
       ⊤ ⊢ ⊥        ... contradiction 1/1
    ```

### Task_43 [1 Point]
  + Sequence: `∃x[P(x) → Q(x)], ¬Q(y) ⊢ ¬P(y)`
  + Model M:
    - Domain-set A = {y}
    - Predicate Q^M = {}
    - Predicate P^M = {(y)}
  + Counter example:
    ```
    ¬Q(y) ⊢ ¬P(y)
       ¬⊥ ⊢ ¬⊤
        ⊤ ⊢ ⊥             ... contradiction 1/2

    ∃x[P(x) → Q(x)] ⊢ ¬P(y)
          ∃x[⊥ → ⊥] ⊢ ¬⊤
                  ⊤ ⊢ ⊥   ... contradiction 2/2

    M ⊭ ¬P(x)
    ```

### Task_44 [1 Point]
  + Sequence: `∃x¬P(x) ⊢ ¬∀xP(x)`
  ```
  1. ∃x¬P(x)            | premise
  ----------------------------------------( Box1
  2. ∀xP(x)             | assumption
  ----------------------------------------( Box2
  3. ¬P(x0)             | x0 fresh
  4. P(x0)              | ∀e 2
  5. ⊥                  | ¬e 3,4
  ----------------------------------------) Box2
  6. ⊥                  | ∃e 1,3-5
  ----------------------------------------) Box1
  7. ¬∀xP(x)            | ¬i 2-7
  ```

### Task_45 [1 Point]
  + Sequence: `∀x∃y[f(x) = y] ⊢ ∀y∃x[f(x) = y]`
  ```
  1. ∀x∃y[f(x) = y]     | premise
  2. ∃y[f(t) = y]       | ∀e 1
  ----------------------------------------( Box1
  3. f(t) = y0          | ∃e 2
  4. ∃x[f(t) = y]       | ∃i 3
  ----------------------------------------) Box1
  5. ∀y∃x[f(x) = y]     | ∀i 3-4
  ```

### Task_46 [1 Point]
  + Sequence: `¬∃xφ(x) ⊢ ∀x¬φ(x)`
  ```
  1. ¬∃xφ(x)            | premise
  ----------------------------------------( Box1
  ----------------------------------------( Box2
  2. φ(x0)              | assumption | x0 fresh
  3. ∃xφ(x)             | ∃i 2 [x/x0]
  4. ⊥                  | ¬e 1,3
  ----------------------------------------) Box2
  5. ¬φ(x0)             | PBC 2-4
  ----------------------------------------) Box1
  6. ∀x¬φ(x)            | ∀i 2-5: [x0 fresh ... Φ[x0/x]]Box ⟹ ∀xφ
  ```

### Task_47 [1 Point]
  + Sequence: `x = y ⊢ f(x,f(x)) = f(y,f(x))`
  - Hint: `f(x,f(x))` and `f(x)` are not the same function, because they have a
    different arity. Therefore you can rewrite the Sequence as following:
  + Sequence: `x = y ⊢ f1(x,f2(x)) = f1(y,f2(x))`
  + Natural Deduction:
    ```
    1. x = y            | premise | t1 = t2
    2. f1(x,f2(x)) = f1(x,f2(x))  | =i: ε ⟹ t = t
    3. f1(x,f2(x)) = f1(y,f2(x))  | =e 1,2: t1 = t2, Φ[t1/x] ⟹ Φ[t2/x]
    3. f1(x,f2(x)) = f1(y,f2(x))  | conclusion
    ```

### Task_48 [1 Point]
  + Sequence: `¬∃x(f(x) = x) ⊢ ∃x∃y¬(x = y)`
  + Model M:
    - Domain-set `A = {}`
    - Function `f^M = f(A) → A`
  + Counter example:
    - premise:
    ```
    ;        ¬=⊤        ;
    ;         |         ;
    ;       ∃x=⊥        ;
    ;         |         ;
    ;        ==⊥        ;
    ;       /   \       ;
    ;      /     \      ;
    ;    f=ε     x=ε    ;
    ;     |             ;
    ;    x=ε            ;
    ```
    - conclusion:
    ```
    ;       ∃x=⊥        ;
    ;         |         ;
    ;       ∃y=⊥        ;
    ;         |         ;
    ;        ¬=⊥        ;
    ;         |         ;
    ;        ==⊤        ;
    ;       /   \       ;
    ;      /     \      ;
    ;    x=ε     y=ε    ;
    ```
    - `⊤ ⊢ ⊥` = counterexample

### Task_49_a [1 Point]
  + Sequence: `∃xP(x) ⊢ P(x)`
    - Note: x on the right side is a free variable, because the Quantifier on left side only applies to the x on the left side of the single-turnstile!
    - Note: x on the left side not the same as x on the right side.
  + Model M:
    - Domain-set A = {a,b}
    - Predicate P^M = {a}
  + Counter example:
    ```
    ∃xP(x) ⊢ P(x)
    ∃xP(a) ⊢ P(b)
         ⊤ ⊢ ⊥    ... contradiction 1/1

    M ⊭ P(x)
    ```

### Task_49_b [1 Point]
  + Sequence: `P(x) ⊢ ∃xP(x)`
  + Model M:
    - Domain-set A = {a,b}
    - Predicate P^M = {a}
  + Counter example:
    ```
    P(x) ⊢ ∃xP(x)
    P(a) ⊢ ∃xP(b)
       ⊤ ⊢ ⊥      ... contradiction 1/1

    M ⊭ P(x)
    ```

### Task_49_c [1 Point]
  + Sequence: `⊢ ∃xP(x)`
    - Note: If the statement has no premise, the left side of the single-turnstile is always true (⊤).
  + Model M:
    - Domain-set A = {a,b}
    - Predicate P^M = {}
  + Counter example:
    ```
    ⊤ ⊢ ∃xP(x)
    ⊤ ⊢ ∃x⊥
    ⊤ ⊢ ⊥      ... contradiction 1/1

    M ⊭ P(x)
    ```

### Task_49_d [1 Point]
  + Sequence: `⊢ ∀xP(x)`
    - Note: If the statement has no premise, the left side of the single-turnstile is always true (⊤).
  + Model M:
    - Domain-set A = {a,b}
    - Predicate P^M = {}
  + Counter example:
    ```
    ⊤ ⊢ ∀xP(x)
    ⊤ ⊢ ∀x⊥
    ⊤ ⊢ ⊥      ... contradiction 1/1

    M ⊭ P(x)
    ```

### Task_50 [2 Points]
  + Sequence: `∀p∀q[p = q → E(p,q)] ⊢ ∀pE(p,p)`
  ```
  1. ∀p∀q[p = q → E(p,q)] | premise
  ----------------------------------------( Box1
  2. ∀q[x0 = q → E(x0,q)] | ∀e 1: ∀pΦ ⟹ Φ[p/x0] | x0 fresh
  3. x0 = x0 → E(x0,x0)   | ∀e 2: ∀qΦ ⟹ Φ[x0/q]
  4. x0 = x0              | =i: ε ⟹ t=t
  5. E(x0,x0)             | →e 4,3: φ, φ → ψ ⟹ ψ
  ----------------------------------------) Box1
  6. ∀pE(p,p)             | ∀i 2-5: [x0 fresh ... Φ[x0/p]]Box ⟹ ∀pφ
  6. ∀pE(p,p)             | conclusion
  ```

### Task_51 [2 Points]
  + Sequence: `∀x[A(x) ∨ B(x)], ∀x¬B(x) ⊢ ∀xA(x)`
    ```
    1. ∀x[A(x) ∨ B(x)]  | premise
    2. ∀x¬B(x)          | premise
    ----------------------------------------( Box1
    3. A(x0) ∨ B(x0)    | ∀e1: ∀xΦ ⟹ Φ[x0/x]  | x0 fresh
    4. ¬B(x0)           | ∀e2: ∀xΦ ⟹ Φ[x0/x]
    ----------------------------------------( Box2
    5. A(x0)            | assumption
    ----------------------------------------) Box2
    ----------------------------------------( Box3
    6. B(x0)            | assumption
    7. ⊥                | ¬e 6,4
    8. A(x0)            | ⊥e 7
    ----------------------------------------) Box3
    9. A(x0)            | ∨e 3, 5-5, 6-8
    ----------------------------------------) Box1
    10. ∀xA(x)          | ∀i 3-9: [x0 fresh ... Φ[x0/x]]Box ⟹ ∀xφ
    10. ∀xA(x)          | conclusion
    ```

### Task_52 [2 Points]
  + Sequence: `∀x[E(x) ∨ O(x)], ∀x[O(x) → E(s(x))] ⊢ ∀x[E(x) ∨ E(s(x))]`
  ```
  1. ∀x[E(x) ∨ O(x)]      | premise
  2. ∀x[O(x) → E(s(x))]   | premise
  ----------------------------------------( Box1
  3. E(x0) ∨ O(x0)        | ∀e 1: ∀xΦ ⟹ Φ[x0/x] | x0 fresh
  4. O(x0) → E(s(x0))     | ∀e 2: ∀xΦ ⟹ Φ[x0/x]
  ----------------------------------------( Box2
  5. E(x0)                | assumption
  6. E(x0) ∨ E(s(x0))     | ∨i_1 5
  ----------------------------------------) Box2
  ----------------------------------------( Box3
  7. O(x0)                | assumption
  8. E(s(x0))             | →e 7,4
  9. E(x0) ∨ E(s(x0))     | ∨i_2 8
  ----------------------------------------) Box3
  10. E(x0) ∨ E(s(x0))    | ∨e 3, 5-6, 7-9
  ----------------------------------------) Box1
  11. ∀x[E(x) ∨ E(s(x))]  | ∀i 3-10: [x0 fresh ... Φ[x0/x]]Box ⟹ ∀xφ
  11. ∀x[E(x) ∨ E(s(x))]  | conclusion
  ```

### Task_53 [Bonus: 2 Points]
  + Sequence: `∀x[E(x) → ¬O(x)], ∀x[¬O(x) → E(x)] ⊢ ∀x[E(x) ∨ O(x)]`
  ```
  01. ∀x[E(x) → ¬O(x)]  | premise
  02. ∀x[¬O(x) → E(x)]  | premise
  ----------------------------------------( Box1
  03. E(x0) → ¬O(x0)    | ∀e 1: ∀xΦ ⟹ Φ[x0/x] | x0 fresh
  04. ¬O(x0) → E(x0)    | ∀e 2: ∀xΦ ⟹ Φ[x0/x]
  05. E(x0) ∨ ¬E(x0)    | LEM
  ----------------------------------------( Box2
  06. E(x0)             | assumption
  07. E(x0) ∨ O(x0)     | ∨i_1 6
  ----------------------------------------) Box2
  ----------------------------------------( Box3
  08. ¬E(x0)            | assumption
  09. ¬O(x0) ∨ ¬¬O(x0)  | LEM
  ----------------------------------------( Box4
  10. ¬Q(x0)            | assumption
  11. E(x0)             | →e 4,10
  12. ⊥                 | ¬e 8,11
  13. E(x0) ∨ Q(x0)     | ⊥e
  ----------------------------------------) Box4
  ----------------------------------------( Box5
  14. ¬¬Q(x0)           | ass
  15. O(x0)             | ¬¬e
  16. E(x0) ∨ O(x0)     | ∨i_2 15
  ----------------------------------------) Box5
  17. E(x0) ∨ O(x0)     | ∨e 9,10-13,14-16
  ----------------------------------------) Box3
  18. E(x0) ∨ O(x0)     | ∨e 5,6-7,8-17
  ----------------------------------------) Box1
  19. ∀x[E(x) ∨ O(x)]   | ∀i 3-18: [x0 fresh ... Φ[x0/x]]Box ⟹ ∀xφ
  ```

### Task_54 [Bonus: 2 Points]
  + Sequence: `∀x[P(x) → ¬Q(x)] ⊢ ¬[∃x[P(x) ∧ Q(x)]]`
  ```
  1. ∀x[P(x) → ¬Q(x)]     | premise
  ----------------------------------------( Box1
  2. ¬¬[∃x[P(x) ∧ Q(x)]]  | assumption
  3. ∃x[P(x) ∧ Q(x)]      | ¬¬e 2
  ----------------------------------------( Box2
  4. P(x0) ∧ Q(x0)        | assumption | x0 fresh
  5. P(x0)                | ∧e_1 4
  6. Q(x0)                | ∧e_2 4
  7. P(x0) → ¬Q(x0)       | ∀e 1: ∀xΦ ⟹ Φ[x0/x]
  8. ¬Q(x0)               | →e 5,7
  9. ⊥                    | ¬e 6,8
  ----------------------------------------) Box2
  10. ⊥                   | ∃e 3,4-9
  ----------------------------------------) Box1
  11. ¬[∃x[P(x) ∧ Q(x)]]  | PBC 2-10
  11. ¬[∃x[P(x) ∧ Q(x)]]  | conclusion
  ```

### Task_55 [Bonus: 2 Points]
  + Sequence: `(x = 0) ∨ (x + x) > 0 ⊢ y = (x + x) → [(y > 0) ∨ (y = (0 + 0))]`
  + Natural Deduction:
  ```
  1. (x = 0) ∨ (x + x) > 0  | premise
  ----------------------------------------( Box1
  2. y = (x + x)  | assumption | t2 = t1 | t2 := y && t1 := x + x
  ----------------------------------------( Box2
  3. x = 0        | assumption
  4. y = (0 + 0)  | =e
  5. (y > 0) ∨ (y = (0 + 0)) | ∨i_2 4
  ----------------------------------------) Box2
  ----------------------------------------( Box3
  6. (x + x) > 0  | assumption | t1 = (x + x)
  7. y > 0        | =e 2,6: t1 = t2, Φ[t1/x] ⟹ Φ[t2/x]
  8. (y > 0) ∨ (y = (0 + 0)) | ∨i_1 7
  ----------------------------------------) Box3
  9. (y > 0) ∨ (y = (0 + 0)) | ∨e 2,3-5,6-8
  ----------------------------------------) Box1
  10. y = (x + x) → [(y > 0) ∨ (y = (0 + 0))] | →i 1,2-9
  ```
