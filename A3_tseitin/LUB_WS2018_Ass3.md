# Logic and Computability WS18,
## Assignment 3



--------------------------------------------------------------------------------
### Task 19 [2 Points]
  In this task, two formulas are given:  
  - `φ: q → (p → r)`
  - `ψ: q → (¬r ∧ ¬p)`

  Check using a truth table if  
  - (a) φ is satisfiable.
  - (b) φ is valid.
  - (c) ψ is satisfiable.
  - (d) ψ is valid.
  - (e) φ entails ψ.
  - (f) ψ entails φ.
  - (g) φ is equivalent to ψ.

### Solution 19
  **Applying De Morgan's law**: `φ: ¬q ∨ ¬p ∨ r`  

  |M|pqr|¬p|¬q|¬q ∨ ¬p ∨ r|φ|
  |-|---|--|--|-----------|-|
  |1|000| 1| 1|     1     |1|
  |2|001| 1| 1|     1     |1|
  |3|010| 1| 0|     1     |1|
  |4|011| 1| 0|     1     |1|
  |5|100| 0| 1|     1     |1|
  |6|101| 0| 1|     1     |1|
  |7|110| 0| 0|     0     |0|
  |8|111| 0| 0|     1     |1|

  - (a) Is φ satisfiable? YES!
  - (b) Is φ valid? NO!



  **Applying De Morgan's law**: `ψ: ¬q ∨ (¬r ∧ ¬p)`  

  |M|pqr|¬p|¬q|¬r|¬r ∧ ¬p|¬q ∨ (¬r ∧ ¬p)|ψ|φ|
  |-|---|--|--|--|-------|--------------|-|-|
  |1|000| 1| 1| 1|   1   |      1       |1|1|
  |2|001| 1| 1| 0|   0   |      1       |1|1|
  |3|010| 1| 0| 1|   1   |      1       |1|1|
  |4|011| 1| 0| 0|   0   |      0       |0|1|
  |5|100| 0| 1| 1|   0   |      1       |1|1|
  |6|101| 0| 1| 0|   0   |      1       |1|1|
  |7|110| 0| 0| 1|   0   |      0       |0|0|
  |8|111| 0| 0| 0|   0   |      0       |0|1|

  - (c) Is ψ satisfiable? YES!
  - (d) Is ψ valid? NO!

  - (e) Does φ entail ψ? NO, because of M4 and M8: `φ ⊬ ψ`
  - (f) Does ψ entail φ? YES: `ψ ⊢ φ`
  - (g) Is φ equivalent to ψ? NO, because last columns in truth tables of φ and ψ are different!



--------------------------------------------------------------------------------
### Task 20 [3 Points]
  Construct a Disjunctive Normal Form (DNF) and a Conjunctive Normal Form (CNF)
  for the following formula from its respective *truth table*.
  - `φ: (b ∨ ¬a) ∧ (c → ¬b)`

### Solution 20
  - Applying De Morgan's law: `φ: (b ∨ ¬a) ∧ (¬c ∨ ¬b)`
  - Phi for WolframAlpha: `((b or not a) and (not c or not b))`

  |abc|¬a|¬b|¬c|b ∨ ¬a|¬c ∨ ¬b|(b ∨ ¬a) ∧ (¬c ∨ ¬b)|   DNF-Cubes  |  CNF-Clauses |
  |---|--|--|--|------|-------|--------------------|--------------|--------------|
  |000| 1| 1| 1|  1   |   1   |         1          |(¬a ∧ ¬b ∧ ¬c)|              |
  |001| 1| 1| 0|  1   |   1   |         1          |(¬a ∧ ¬b ∧  c)|              |
  |010| 1| 0| 1|  1   |   1   |         1          |(¬a ∧  b ∧ ¬c)|              |
  |011| 1| 0| 0|  1   |   0   |         0          |              |( a ∨ ¬b ∨ ¬c)|
  |100| 0| 1| 1|  0   |   1   |         0          |              |(¬a ∨  b ∨  c)|
  |101| 0| 1| 0|  0   |   1   |         0          |              |(¬a ∨  b ∨ ¬c)|
  |110| 0| 0| 1|  1   |   1   |         1          |( a ∧  b ∧ ¬c)|              |
  |111| 0| 0| 0|  1   |   0   |         0          |              |(¬a ∨ ¬b ∨ ¬c)|

  - DNF: `(¬a ∧ ¬b ∧ ¬c) ∨ (¬a ∧ ¬b ∧  c) ∨ (¬a ∧  b ∧ ¬c) ∨ ( a ∧  b ∧ ¬c)`
  - CNF: `( a ∨ ¬b ∨ ¬c) ∧ (¬a ∨  b ∨  c) ∧ (¬a ∨  b ∨ ¬c) ∧ (¬a ∨ ¬b ∨ ¬c)`

  - DNF for WolframAlpha: `((not a and not b and not c) or (not a and not b and  c) or (not a and  b and not c) or ( a and  b and not c))`
  - CNF for WolframAlpha: `(a and not b and not c) and (not a and b and c) and (not a and b and not c) and (not a and not b and not c)`



--------------------------------------------------------------------------------
### Task 21 [2 Points]
  + The CNF rewriting rules for `AND`, `OR` and `NOT` are:  
    - AND: `(q ↔ (a ∧ b)) ≡ (¬q ∨ a) ∧ (¬q ∨ b) ∧ (¬a ∨ ¬b ∨ q)`
    - OR:  `(q ↔ (a ∨ b)) ≡ (¬q ∨ a ∨ b) ∧ (¬a ∨ q) ∧ (¬b ∨ q)`
    - NOT: `(q ↔ ¬a) ≡ (q ∨ a) ∧ (¬q ∨ ¬a)`
  + respectively. Give a CNF rewriting rule for:  
    - (a) `XNOR`, i.e. `(q ↔ ¬(a ⊕ b)) ≡ ?`
    - (b) `NAND`, i.e. `(q ↔ (a NAND b)) ≡ ?`

### Solution 21

  + CNF derivation of logical AND:
    - Equivalence: `q ↔ (a ∧ b)`
    - Derivation:
      ```
      q ↔ (a ∧ b)
      (q → (a ∧ b)) ∧ ((a ∧ b) → q)
      (¬q ∨ (a ∧ b)) ∧ (¬(a ∧ b) ∨ q)
      (¬q ∨ (a ∧ b)) ∧ (¬a ∨ ¬b ∨ q)
      Distributivgesetz:
      (¬q ∨ a) ∧ (¬q ∧ b) ∧ (¬a ∨ ¬b ∨ q)
      ```
    - Conclusion: `(q ↔ (a ∧ b)) ≡ (¬q ∨ a) ∧ (¬q ∨ b) ∧ (¬a ∨ ¬b ∨ q)`

  + CNF derivation of logical OR:
    - Equivalence: `q ↔ (a ∨ b)`
    - Derivation:
      ```
      q ↔ (a ∨ b)
      (q → (a ∨ b)) ∧ ((a ∨ b) → q)
      (¬q ∨ (a ∨ b)) ∧ (¬(a ∨ b) ∨ q)
      (¬q ∨ a ∨ b) ∧ ((¬a ∧ ¬b) ∨ q)
      (¬q ∨ a ∨ b) ∧ ((¬a ∨ q) ∧ (¬b ∨ q))
      (¬q ∨ a ∨ b) ∧ (¬a ∨ q) ∧ (¬b ∨ q)
      ```
    - Conclusion: `(q ↔ (a ∨ b)) ≡ (¬q ∨ a ∨ b) ∧ (¬a ∨ q) ∧ (¬b ∨ q)`

  + CNF derivation of logical NOT:
    - Equivalence: `q ↔ ¬a`
    - Derivation:
      ```
      q ↔ ¬a
      (¬a → q) ∧ (q → ¬a)
      (a ∨ q) ∧ (¬q ∨ ¬a)
      ```
    - Conclusion: `(q ↔ ¬a) ≡ (a ∨ q) ∧ (¬q ∨ ¬a)`

  + CNF derivation of logical NOR:
    - Equivalence: `q ↔ ¬(a ∨ b)`
    - Derivation:
      ```
      q ↔ ¬(a ∨ b)
      q ↔ (¬a ∧ ¬b)
      (q → (¬a ∧ ¬b)) ∧ ((¬a ∧ ¬b) → q)
      (¬q ∨ (¬a ∧ ¬b)) ∧ (¬(¬a ∧ ¬b) ∨ q)
      ((¬q ∨ ¬a) ∧ (¬q ∨ ¬b)) ∧ ((a ∨ b) ∨ q)
      (¬q ∨ ¬a) ∧ (¬q ∨ ¬b) ∧ (a ∨ b ∨ q)
      ```
    - Conclusion: `(q ↔ (a ⊕ b)) ≡ ?`

  + CNF derivation of logical XOR:
    - Equivalence: `q ↔ (a ⊕ b)`
    - Derivation:
      ```
      q ↔ (a ⊕ b)
      q ↔ ((a ∧ ¬b) ∨ (¬a ∧ b))
      (q → ((a ∧ ¬b) ∨ (¬a ∧ b))) ∧ (((a ∧ ¬b) ∨ (¬a ∧ b)) → q)
      (¬q ∨ ((a ∧ ¬b) ∨ (¬a ∧ b))) ∧ (¬((a ∧ ¬b) ∨ (¬a ∧ b)) ∨ q)
      (¬q ∨ ((a ∧ ¬b) ∨ (¬a ∧ b))) ∧ ((¬(a ∧ ¬b) ∧ ¬(¬a ∧ b)) ∨ q)
      (¬q ∨ ((a ∧ ¬b) ∨ (¬a ∧ b))) ∧ (((¬a ∨ b) ∧ (a ∨ ¬b)) ∨ q)
      (¬q * ((a + ¬b) * (¬a + b))) + (((¬a * b) + (a * ¬b)) * q)
      (¬q * ((a * ¬a) + (a * b) + (¬b * ¬a) + (¬b * b))) + (((¬a * b) * q) + ((a * ¬b) * q))
      (((¬q * a * ¬a) + (¬q * a * b) + (¬q * ¬b * ¬a) + (¬q * ¬b * b))) + (((q * ¬a * b) + (q * a * ¬b)))
      (¬q * a * ¬a) + (¬q * a * b) + (¬q * ¬b * ¬a) + (¬q * ¬b * b) + (q * ¬a * b) + (q * a * ¬b)
      (¬q ∨ a ∨ ¬a) ∧ (¬q ∨ a ∨ b) ∧ (¬q ∨ ¬b ∨ ¬a) ∧ (¬q ∨ ¬b ∨ b) ∧ (q ∨ ¬a ∨ b) ∧ (q ∨ a ∨ ¬b)

      WolframAlpha: ((not q or a or not a) and (not q or a or b) and (not q or not b or not a) and (not q or not b or b) and (q or not a or b) and (q or a or not b))
      Result of logictools.org is: ( q ∨ a ∨ ¬a) ∧ ( q ∨ a ∨ b) ∧ ( q ∨ ¬b ∨ ¬a) ∧ (¬q ∨ ¬a ∨ b) ∧ (¬q ∨ a ∨ ¬b) ∧ (q ∨ ¬b ∨ b)
      logictools.org WolframAlpha: (( q or a or not a) and ( q or a or b) and ( q or not b or not a) and (not q or not a or b) and (not q or a or not b) and (q or not b or b))
      ```
    - Conclusion: `(q ↔ (a ⊕ b)) ≡ ((¬q ∨ a ∨ ¬a) ∧ (¬q ∨ a ∨ b) ∧ (¬q ∨ ¬b ∨ ¬a) ∧ (¬q ∨ ¬b ∨ b) ∧ (q ∨ ¬a ∨ b) ∧ (q ∨ a ∨ ¬b))`

  + (a) CNF derivation of logical XNOR:
    - Equivalence: `q ↔ ¬(a ⊕ b)`
    - Derivation:
      ```
      q ↔ ¬(a ⊕ b)
      q ↔ ¬((a ∧ ¬b) ∨ (¬a ∧ b))
      q ↔ (¬(a ∧ ¬b) ∧ ¬(¬a ∧ b))
      q ↔ ((¬a ∨ b) ∧ (a ∨ ¬b))
      ((q → (¬a ∨ b)) ∧ (q → (a ∨ ¬b))) ∧ (((¬a ∨ b) → q) ∧ ((a ∨ ¬b) → q))
      ((¬q ∨ (¬a ∨ b)) ∧ (¬q ∨ (a ∨ ¬b))) ∧ ((¬(¬a ∨ b) ∨ q) ∧ (¬(a ∨ ¬b) ∨ q))
      ((¬q ∨ ¬a ∨ b) ∧ (¬q ∨ a ∨ ¬b)) ∧ (((a ∧ ¬b) ∨ q) ∧ ((¬a ∧ b) ∨ q))
      ( ¬q * ¬a * b) + (¬q * a * ¬b)  + ( (a + ¬b) * q) + ((¬a + b) * q)
      ( ¬q * ¬a * b) + (¬q * a * ¬b)  + (a * q) + (¬b * q) + (¬a * q) + (b * q)
      (¬q ∨ ¬a ∨ b) ∧ (¬q ∨ a ∨ ¬b) ∧ (a ∨ q) ∧ (¬b ∨ q) ∧ (¬a ∨ q) ∧ (b ∨ q)
      ```
    - Conclusion: `(q ↔ ¬(a ⊕ b)) ≡ (¬q ∨ ¬a ∨ b) ∧ (¬q ∨ a ∨ ¬b) ∧ (a ∨ q) ∧ (¬b ∨ q) ∧ (¬a ∨ q) ∧ (b ∨ q)`

  + (b) CNF derivation of logical NAND:
    - Equivalence: `q ↔ (a NAND b)`
    - Derivation:
      ```
      q ↔ (a NAND b)
      q ↔ ¬(a ∧ b)
      q ↔ (¬a ∨ ¬b)
      (q → (¬a ∨ ¬b)) ∧ ((¬a ∨ ¬b) → q)
      (¬q ∨ (¬a ∨ ¬b)) ∧ (¬(¬a ∨ ¬b) ∨ q)
      (¬q ∨ ¬a ∨ ¬b) ∧ ((a ∧ b) ∨ q)
      Distributivgesetz:
      (¬q ∨ ¬a ∨ ¬b) ∧ (a ∨ q) ∧ (b ∨ q)
      ```
    - Conclusion: `(q ↔ (a NAND b)) ≡ (¬q ∨ ¬a ∨ ¬b) ∧ (a ∨ q) ∧ (b ∨ q)`



--------------------------------------------------------------------------------
### Task 22 [2 Points]
  Translate Circuit 1 from Figure 1 into CNF using Tseitin’s encoding.  
  ```
  ;   a ────────────────┐  ╔═════╗
  ;         ╔═════╗  p  └──╢ OR  ║   q   ╔═════╗
  ;   b ────╢ NOT ╟────────╢     ╟───────╢ AND ╟────── r
  ;         ╚═════╝        ╚═════╝   ┌───╢     ║
  ;   c ─────────────────────────────┘   ╚═════╝
  ;
  ; Figure 1: Circuit 1
  ```

### Solution 22
  - Circuit logic: `φ = (a ∨ ¬b) ∧ c`
  - Set new variables:
    ```
    x1 ↔ ¬b
    x2 ↔ a ∨ x1
    x3 ↔ ¬x2
    x4 ↔ x3 ∧ c
    ```
  - Setup CNF:
    ```
    CNF(φ) = x4 ∧
            (x4 ↔ (x3 ∧ c)) ∧
            (x3 ↔ ¬x2) ∧
            (x2 ↔ (a ∨ x1)) ∧
            (x1 ↔ ¬b)
    ```
  + Tseitin Rules:
    - OR:  `χ ↔ (φ ∨ ψ) ⇔ (¬φ ∨ χ) ∧ (¬ψ ∨ χ) ∧ (¬χ ∨ φ ∨ ψ)`
    - AND: `χ ↔ (φ ∧ ψ) ⇔ (¬χ ∨ φ) ∧ (¬χ ∨ ψ) ∧ (¬φ ∨ ¬ψ ∨ χ)`
    - NOT: `χ ↔ ¬φ ⇔ (¬χ ∨ ¬φ) ∧ (φ ∨ χ)`
  - Applying Tseitin Rules:
    ```
    CNF(φ) = x4 ∧
            (¬x4 ∨ x3) ∧ (¬x4 ∨ c) ∧ (¬x3 ∨ ¬c ∨ x4) ∧
            (¬x3 ∨ ¬x2) ∧ (x2 ∨ x3) ∧
            (¬a ∨ x2) ∧ (¬x1 ∨ x2) ∧ (¬x2 ∨ a ∨ x1) ∧
            (¬x1 ∨ ¬b) ∧ (b ∨ x1)
    ```



--------------------------------------------------------------------------------
### Task 23 [3 Points]
  Translate Circuit 2 from Figure 2 into CNF using Tseitin’s encoding.  
  ```
  ;              ╔═════╗    s    ╔═════╗   u
  ;   a ─────────╢ AND ╟─────────╢ NOT ╟───────┐
  ;         ┌────╢     ║         ╚═════╝       │  ╔═════╗   w   ╔═════╗       
  ;         │    ╚═════╝                       └──╢ AND ╟───────╢ NOT ╟──── x
  ;   b ────┼─────────────────┐                ┌──╢     ║       ╚═════╝       
  ;         │                 │  ╔═════╗   v   │  ╚═════╝    
  ;         │    ╔═════╗    t └──╢ OR  ╟───────┘
  ;   c ────┴────╢ NOT ╟─────────╢     ║
  ;              ╚═════╝         ╚═════╝
  ; Figure 2: Circuit 2
  ```

### Solution 23
  - Circuit logic: `φ = ¬(¬(a ∧ c) ∧ (b ∨ ¬c))`
  - Set new variables:
    ```
    x1 ↔ a ∧ c
    x2 ↔ ¬c
    x3 ↔ ¬x1
    x4 ↔ b ∨ x2
    x5 ↔ x3 ∧ x4
    x6 ↔ ¬x5
    ```
  - Setup CNF:
    ```
    CNF(φ) = x6 ∧
            (x6 ↔ ¬x5) ∧
            (x5 ↔ x3 ∧ x4) ∧
            (x4 ↔ b ∨ x2) ∧
            (x3 ↔ ¬x1) ∧
            (x2 ↔ ¬c) ∧
            (x1 ↔ a ∧ c)
    ```
  + Tseitin Rules:
    - OR:  `χ ↔ (φ ∨ ψ) ⇔ (¬φ ∨ χ) ∧ (¬ψ ∨ χ) ∧ (¬χ ∨ φ ∨ ψ)`
    - AND: `χ ↔ (φ ∧ ψ) ⇔ (¬χ ∨ φ) ∧ (¬χ ∨ ψ) ∧ (¬φ ∨ ¬ψ ∨ χ)`
    - NOT: `χ ↔ ¬φ ⇔ (¬χ ∨ ¬φ) ∧ (φ ∨ χ)`
  - Applying Tseitin Rules:
    ```
    CNF(φ) = x6 ∧
            (¬x6 ∨ ¬x5) ∧ (x5 ∨ x6) ∧
            (¬x5 ∨ x3) ∧ (¬x5 ∨ x4) ∧ (¬x3 ∨ ¬x4 ∨ x5) ∧
            (¬b ∨ x4) ∧ (¬x2 ∨ x4) ∧ (¬x4 ∨ b ∨ x2) ∧
            (¬x3 ∨ ¬x1) ∧ (x1 ∨ x3) ∧
            (¬x2 ∨ ¬c) ∧ (c ∨ x2) ∧
            (¬x1 ∨ a) ∧ (¬x1 ∨ c) ∧ (¬a ∨ ¬c ∨ x1)
    ```



--------------------------------------------------------------------------------
### Precosat Introduction
  - In the following examples we will use the SAT solver precosat.
  + Short instructions on precosat:
    - Setting up precosat:
      ```
      $ ## Connect to pluto.tugraz.at via ssh and login with your student account:
      $ ssh TUonlineNickname@pluto.tugraz.at
      $ ## Get _precosat_ from the website with the following command:
      $ wget http://fmv.jku.at/precosat/precosat236.zip
      $ ## To compile precosat, type:  
      $ unzip precosat236.zip
      $ cd precosat236/
      $ ./configure
      $ make
      $ ./precosat -h
      ```
    - precosat reads files in dimacs format.  
    - E.g.: `(a ∨ b) ∧ (¬b ∨ c)` translated into dimacs format looks like this:  
      ```
      c some coment
      p cnf 3 2
      1 2 0
      -2 3 0
      ```
    - Lines starting with
      c are a comment,
      p starts the problem description,
      cnf is the problem type,
      3 is the number of variables,
      2 is the number of clauses,
      the next two lines are the cnf clauses.
      The clauses are separated with `0` and a newline `\n`.
    - Ask Google for more examples.
    - Write your CNF formula into a file `matnr.cnf`.
    - Start the SAT solver with `./precosat matnr.cnf`.



--------------------------------------------------------------------------------
### Task 24 [3 Points]
  - In this example we want to check equivalence of the two circuits given in Figure 1 and Figure 2.
  - Construct a CNF formula to check equivalence for the two given circuits by using the CNF formulas from the previous examples.
    (Hint: Maybe you can use any rewrite rule from task 21?)
  - Use precosat to check satisfiability of these formulas.
  - **Upload the resulting .cnf file to STicS.**

### Solution 24
  - Figure 1: `φ = (a ∨ ¬b) ∧ c`
  - Figure 2: `ψ = ¬(¬(a ∧ c) ∧ (b ∨ ¬c))`
  - Resulting `Φ = ((a ∨ ¬b) ∧ c) ⊕ ¬(¬(a ∧ c) ∧ (b ∨ ¬c))`

  + Tseitin Rules:
    - OR:  `χ ↔ (φ ∨ ψ) ⇔ (¬φ ∨ χ) ∧ (¬ψ ∨ χ) ∧ (¬χ ∨ φ ∨ ψ)`
    - AND: `χ ↔ (φ ∧ ψ) ⇔ (¬χ ∨ φ) ∧ (¬χ ∨ ψ) ∧ (¬φ ∨ ¬ψ ∨ χ)`
    - NOT: `χ ↔ ¬φ ⇔ (¬χ ∨ ¬φ) ∧ (φ ∨ χ)`
    - XOR: `χ ↔ (φ ⊕ ψ) ≡ (¬χ ∨ φ ∨ ¬φ) ∧ (¬χ ∨ φ ∨ ψ) ∧ (¬χ ∨ ¬ψ ∨ ¬φ) ∧ (¬χ ∨ ¬ψ ∨ ψ) ∧ (χ ∨ ¬φ ∨ ψ) ∧ (χ ∨ φ ∨ ¬ψ)`

  | New Variables | Tseitin Rule Type | Applied Tseitin Rules |
  | ------------- | ----------------- | -------------------- |
  | x1 ↔ ¬b       | NOT | (¬x1 ∨ ¬b) ∧ (b ∨ x1) |
  | x2 ↔ ¬c       | NOT | (¬x2 ∨ ¬c) ∧ (c ∨ x2) |
  | x3 ↔ a ∨ x1   | OR  | (¬a ∨ x3) ∧ (¬x1 ∨ x3) ∧ (¬x3 ∨ a ∨ x1) |
  | x4 ↔ b ∨ x2   | OR  | (¬b ∨ x4) ∧ (¬x2 ∨ x4) ∧ (¬x4 ∨ b ∨ x2) |
  | x5 ↔ x3 ∧ c   | AND | (¬x5 ∨ x3) ∧ (¬x5 ∨ c) ∧ (¬x3 ∨ ¬c ∨ x5) |
  | x6 ↔ a ∧ c    | AND | (¬x6 ∨ a) ∧ (¬x6 ∨ c) ∧ (¬a ∨ ¬c ∨ x6) |
  | x7 ↔ ¬x6      | NOT | (¬x7 ∨ ¬x6) ∧ (x6 ∨ x7) |
  | x8 ↔ x7 ∧ x4  | AND | (¬x8 ∨ x7) ∧ (¬x8 ∨ x4) ∧ (¬x7 ∨ ¬x4 ∨ x8) |
  | x9 ↔ ¬x8      | NOT | (¬x9 ∨ ¬x8) ∧ (x8 ∨ x9) |
  | x10 ↔ x5 ⊕ x9 | XOR | (¬x10 ∨ x5 ∨ ¬x5) ∧ (¬x10 ∨ x5 ∨ x9) ∧ (¬x10 ∨ ¬x9 ∨ ¬x5) ∧ (¬x10 ∨ ¬x9 ∨ x9) ∧ (x10 ∨ ¬x5 ∨ x9) ∧ (x10 ∨ x5 ∨ ¬x9) |
  | x10           |  -  | - |

  - Rewriting applied tseitin rules in reversed order:
    ```
    (x10) ∧
    (¬x10 ∨ x5 ∨ ¬x5) ∧ (¬x10 ∨ x5 ∨ x9) ∧ (¬x10 ∨ ¬x9 ∨ ¬x5) ∧ (¬x10 ∨ ¬x9 ∨ x9) ∧ (x10 ∨ ¬x5 ∨ x9) ∧ (x10 ∨ x5 ∨ ¬x9) ∧
    (¬x9 ∨ ¬x8) ∧ (x8 ∨ x9) ∧
    (¬x8 ∨ x7) ∧ (¬x8 ∨ x4) ∧ (¬x7 ∨ ¬x4 ∨ x8) ∧
    (¬x7 ∨ ¬x6) ∧ (x6 ∨ x7) ∧
    (¬x6 ∨ a) ∧ (¬x6 ∨ c) ∧ (¬a ∨ ¬c ∨ x6) ∧
    (¬x5 ∨ x3) ∧ (¬x5 ∨ c) ∧ (¬x3 ∨ ¬c ∨ x5) ∧
    (¬b ∨ x4) ∧ (¬x2 ∨ x4) ∧ (¬x4 ∨ b ∨ x2) ∧
    (¬a ∨ x3) ∧ (¬x1 ∨ x3) ∧ (¬x3 ∨ a ∨ x1) ∧
    (¬x2 ∨ ¬c) ∧ (c ∨ x2) ∧
    (¬x1 ∨ ¬b) ∧ (b ∨ x1)
    ```
  - Rewriting CNF in precosat format:
    ```
    cat <<EOT>> ./matriculanumber.cnf
    c Previous  variables: x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, a, b, c
    c Enumerate variables:  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,11,12,13
    c Number of variables: 13
    c Number of clauses: 30
    c Syntax: p <problem_type> <number_of_variables> <number_of_clauses>
    p cnf 13 30
    10 0
    -10 5 -5 0
    -10 5 9 0
    -10 -9 -5 0
    -10 -9 9 0
    10 -5 9 0
    10 5 -9 0
    -9 -8 0
    8 9 0
    -8 7 0
    -8 4 0
    -7 -4 8 0
    -7 -6 0
    6 7 0
    -6 11 0
    -6 13 0
    -11 -13 6 0
    -5 3 0
    -5 13 0
    -3 -13 5 0
    -12 4 0
    -2 4 0
    -4 12 2 0
    -11 3 0
    -1 3 0
    -3 11 1 0
    -2 -13 0
    13 2 0
    -1 -12 0
    12 1 0
    EOT
    ```
    - Executing precosat with given dimacs-input-file as first argument:
    ```
    $ ssh TUonlineNickname@pluto.tugraz.at
    $ ./precosat ./matriculanumber.cnf | grep "UNSATISFIABLE\|SATISFIABLE"
    ```
    + Result:
      - If result is `UNSATISFIABLE` both CNFs are equivalent!
      - If result is `SATISFIABLE` both CNFs are NOT equivalent!
      - In case of task 24, the result is `UNSATISFIABLE` and therefore the two CNFs are equivalent!
      + Validated result by https://www.wolframalpha.com/
        - search-bar-input: ((a or not b) and c) xor (not (not (a and c) and (b or not c)))
        - All models in truth-table are `false`, therefore `UNSATISFIABLE`
