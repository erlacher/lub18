# Logic and Computability WS18 | Assignment 1 | November 9th 2018  

**Hint:** If you are using the Atom-Editor, you can open the markdown  
preview for this file by using the key-combination [ctrl]+[alt]+[m].  



--------------------------------------------------------------------------------  
## Task 1  
--------------------------------------------------------------------------------  
  Use ¬, →, ∧, and ∨ to express the following declarative sentences in  
  propositional logic as detailed as possible. In each case state what your  
  respective propositional atoms p, q, etc. mean.  
  + (a) [1 Point]  
    - i. Students will pass the exam, if and only if they prepare appropriately.  
    - ii. If it rains and you don’t have an umbrella, you will get wet.  
  + (b) [1 Point]  
    - i. Students have to present their solutions at the blackboard.  
    - ii. Tomorrow is Saturday, if and only if today is Friday.  
  + (c) [1 Point]  
    - i. In the evening Bob will either study or play video games, but not both.  
    - ii. If it’s summer, then if the sun is shining in the morning, birds will be singing.  

--------------------------------------------------------------------------------  
### 1.a.i Solution:  
  Declarative sentence: "Students will pass the exam, if and only if they prepare appropriately."  
  Used propositional atoms: `a` and `b`:  
  a ... Students will pass the exam.  
  b ... Students prepare appropriately.  
  Propositional logic statement: `b ⟺ a`.  

### 1.a.ii Solution:  
  Declarative sentence: "If it rains and you don’t have an umbrella, you will get wet."  
  Used propositional atoms: `a`, `b` and `c`:  
  a ... It rains.  
  b ... You do have an umbrella.  
  c ... You get wet.  
  Propositional logic statement: `(a ∧ ¬b) → c`.  

### 1.b.i Solution:  
  Declarative sentence: "Students have to present their solutions at the blackboard."  
  Used propositional atoms: `a`:  
  a ... Students have to present their solutions at the blackboard.  
  Propositional logic statement: `a`.  

### 1.b.ii Solution:  
  Declarative sentence: "Tomorrow is Saturday, if and only if today is Friday."  
  Used propositional atoms: `a` and `b`:  
  a ... Tomorrow is Saturday.  
  b ... Today is Friday.  
  Propositional logic statement: `a ⟺ b`.  

### 1.c.i Solution:  
  Declarative sentence: "In the evening Bob will either study or play video games, but not both."  
  Used propositional atoms: `a`, `b` and `c`:  
  a ... In the evening.  
  b ... Bob will study.  
  c ... Bob will play.  
  Propositional logic statement: `a ∧ (b XOR c)` == `a ∧ (b ⊻ c)` == `a ∧ (b ⊕ c)`.  

### 1.c.ii Solution:  
  Declarative sentence: "If it’s summer, then if the sun is shining in the morning, birds will be singing."  
  Used propositional atoms: `a`, `b` and `c`:  
  a ... It is summer.  
  b ... Sun is shining in the morning.  
  c ... Birds do sing.  
  Propositional logic statement: `(a ∧ b) → c`.  



--------------------------------------------------------------------------------  
## Task 2  
--------------------------------------------------------------------------------  
  A formula is valid `iff` it computes `T` for all its valuations and it  
  is satisfiable `iff` it computes `T` for at least one of its valuations.  
  For each of the following formulas:  
  - Show whether the formula is *valid*.  
  - Show whether the formula is *satisfiable*.  
  - Use a *truth table* to solve the tasks.  
  (a) [1 Point] (p ∨ q) → (¬p ∧ q)  
  (b) [1 Point] (¬p ∧ q) → (p → p ∨ q)  
  (c) [2 Points] ¬((p ↔ ¬q) ∧ (q ↔ r) ∧ (r ↔ ¬p)) → (p ∧ q ∧ r)  

--------------------------------------------------------------------------------  
### 2.a Solution:  
  Given statement: `ϕ = (p ∨ q) → (¬p ∧ q)`  
  Atomic propositions: `p` and `q`.  
  Truth-table:  
  ```  
  |pq|p ∨ q|¬p|¬p ∧ q| ϕ |  
  |00|  0  | 1|   0  | 1 |  
  |01|  0  | 1|   1  | 1 |  
  |10|  0  | 0|   0  | 1 |  
  |11|  1  | 0|   0  | 0 |  
  ```  
  + Is formula `ϕ` satisfiable?  
    - Yes, because there exists models, where `ϕ` evaluates to true!  
  + Is formula `ϕ` valid?
    - No, because the model M (p=1, q=1) evaluates to false!  

### 2.b Solution:  
  Given statement: `ϕ = (¬p ∧ q) → (p → (p ∨ q))`  
  Substitution:    `ϕ = A → (p → B)`  
  Substitution:    `ϕ = A → C`  
  Atomic propositions: `p` and `q`.  
  Truth-table:  
  ```  
  |  |  |   A  |  B  |  C  |  ϕ  |  
  |pq|¬p|¬p ∧ q|p ∨ q|p → B|A → C|  
  |00| 1|   0  |  0  |  1  |  1  |  
  |01| 1|   1  |  0  |  1  |  1  |  
  |10| 0|   0  |  0  |  0  |  1  |  
  |11| 0|   0  |  1  |  1  |  1  |  
  ```  
  + Is formula `ϕ` satisfiable?  
    - Yes, because there exists models, where `ϕ` evaluates to true!  
  + Is formula `ϕ` valid?
    - Yes, because every model evaluates to true!  

### 2.c Solution:  
  Given statement: `ϕ = ¬((p ↔ ¬q) ∧ (q ↔ r) ∧ (r ↔ ¬p)) → (p ∧ q ∧ r)`  
  Substitution:    `¬(A ∧ B ∧ C) → D` ⟺ `¬E → D` ⟺ `F → D`  
  Atomic propositions: `p`, `q` and `r`.  
  Truth-table:  
  ```  
  |   |  |  |  A   |  B  |  C   |    D    |    E    | F|  ϕ  |  
  |pqr|¬p|¬q|p ↔ ¬q|q ↔ r|r ↔ ¬p|p ∧ q ∧ r|A ∧ B ∧ C|¬E|F → D|  
  |000| 1| 1|  0   |  1  |  0   |    0    |    0    | 1|  0  |  
  |001| 1| 1|  0   |  0  |  1   |    0    |    0    | 1|  0  |  
  |010| 1| 0|  1   |  1  |  0   |    0    |    0    | 1|  0  |  
  |011| 1| 0|  1   |  0  |  1   |    0    |    0    | 1|  0  |  
  |100| 0| 1|  1   |  0  |  1   |    0    |    0    | 1|  0  |  
  |101| 0| 1|  1   |  1  |  0   |    0    |    0    | 1|  0  |  
  |110| 0| 0|  0   |  0  |  1   |    0    |    0    | 1|  0  |  
  |111| 0| 0|  0   |  1  |  0   |    1    |    0    | 1|  1  |  
  ```  
  + Is formula `ϕ` satisfiable?  
    - Yes, because there exists one model, where `ϕ` evaluates to true!  
  + Is formula `ϕ` valid?
    - No, because several models evaluate to false!  



--------------------------------------------------------------------------------  
## Task 3  
--------------------------------------------------------------------------------  
  Draw a parse tree for the following formulas and use the tree to check  
  for both formulas if the assignment `p = F`, `q = T`, `r = F` makes the  
  formula true or not. Additionally, find a model which evaluates to the  
  opposite of the given model.  
  (a) [1 Points] (p ∧ r) → ((¬p ∨ q) ∧ (p → q))  
  (b) [1 Points] (¬r → (p ∨ q)) ∧ (¬r ∧ (p → ¬p))  

### 3.a Solution:  
  Statement: `ϕ = (p ∧ r) → ((¬p ∨ q) ∧ (p → q))`  
  Model M1:  `p = F`, `q = T`, `r = F`.  
  Parse-tree:  
  ```  
  ;            ϕ                 ;            1                 ;  
  ;            |                 ;            |                 ;  
  ;           (→)                ;          (0→1)               ;  
  ;       ____/ \____            ;       ____/ \____            ;  
  ;      /           \           ;      /           \           ;  
  ;    (∧)           (∧)         ;   (0∧0)         (1∧1)        ;  
  ;    / \         __/ \__       ;    / \         __/ \__       ;  
  ;   p   r       /       \      ;   0   0       /       \      ;  
  ;             (∨)       (→)    ;            (1∨1)     (0→1)   ;  
  ;             / \       / \    ;             / \       / \    ;  
  ;           (¬)  q     p   q   ;          (¬0)  1     0   1   ;  
  ;            |                 ;            |                 ;  
  ;            p                 ;            0                 ;  
  ```  
  Parse-tree result: `ϕ^M1 = T`  
  I.e.: The formula `ϕ` evaluates to `True`, by given model `M1`.  
  An alternative notation of `ϕ^M1 = T` is `M1 ⊧ ϕ`.  
  I.e.: Model `M1` satisfies formula `ϕ`.  


  **Additionally part:**  
  Model M2:  `p = T`, `q = F`, `r = T`.  
  Parse-tree:  
  ```  
  ;            0                 ;  
  ;            |                 ;  
  ;          (1→0)               ;  
  ;       ____/ \____            ;  
  ;      /           \           ;  
  ;   (1∧1)         (0∧0)        ;  
  ;    / \         __/ \__       ;  
  ;   1   1       /       \      ;  
  ;            (0∨0)     (1→0)   ;  
  ;             / \       / \    ;  
  ;          (¬1)  0     1   0   ;  
  ;            |                 ;  
  ;            1                 ;  
  ```  
  Parse-tree result: `ϕ^M2 = F`  
  The formula `ϕ` evaluates to `False`, by given model M2.  
  An alternative notation of `ϕ^M2 = F` is `M2 ⊧ ϕ`.  
  I.e.: Model `M2` falsifies formula `ϕ`.  
--------------------------------------------------------------------------------  


### 3.b Solution:  
  Statement: `ϕ = (¬r → (p ∨ q)) ∧ (¬r ∧ (p → ¬p))`  
  Model M1:  `p = F`, `q = T`, `r = F`.  
  ```  
  ;             ϕ               ;             1               ;  
  ;             |               ;             |               ;  
  ;            (∧)              ;           (1∧1)             ;  
  ;         ___/ \___           ;         ___/ \___           ;  
  ;        /         \          ;        /         \          ;  
  ;      (→)         (∧)        ;     (1→1)       (1∧1)       ;  
  ;      / \         / \        ;      / \         / \        ;  
  ;     /   \       /   \       ;     /   \       /   \       ;  
  ;   (¬)   (∨)   (¬)   (→)     ;  (¬0)  (0∨1) (¬0)  (0→1)    ;  
  ;    |    / \    |    / \     ;    |    / \    |    / \     ;  
  ;    r   p   q   r   p  (¬)   ;    0   0   1   0   0 (¬0)   ;  
  ;                        |    ;                        |    ;  
  ;                        p    ;                        0    ;  
  ```  
  Parse-tree result: `ϕ^M1 = T`  
  The formula `ϕ` evaluates to `True`, by given model M1.  
  An alternative notation of `ϕ^M1 = T` is `M1 ⊧ ϕ`.  
  I.e.: Model `M1` satisfies formula `ϕ`.

  **Additionally part:**  
  Model M2:  `p = F`, `q = F`, `r = F`.  
  Parse-tree:  
  ```  
  ;             0               ;  
  ;             |               ;  
  ;           (0∧1)             ;  
  ;         ___/ \___           ;  
  ;        /         \          ;  
  ;     (1→0)       (1∧1)       ;  
  ;      / \         / \        ;  
  ;     /   \       /   \       ;  
  ;  (¬0)  (0∨0) (¬0)  (0→1)    ;  
  ;    |    / \    |    / \     ;  
  ;    0   0   0   0   0 (¬0)   ;  
  ;                        |    ;  
  ;                        0    ;  
  ```  
  Parse-tree result: `ϕ^M2 = F`  
  The formula `ϕ` evaluates to `False`, by given model M2.  
  An alternative notation of `ϕ^M2 = F` is `M2 ⊧ ϕ`.  
  I.e.: Model `M2` falsifies formula `ϕ`.  



--------------------------------------------------------------------------------  
## Task 4  
--------------------------------------------------------------------------------  
  [6 Points] In this task you have to describe the Latin Square Puzzle¹  
  using propositional logic. In the Latin Square Puzzle one has to color  
  cells in an (n×n) grid such that there is exactly one colored cell in  
  each row and each column. Furthermore, colored cells must not be  
  adjacent to each other (also not diagonally). Numbers contained in  
  certain cells of the grid indicate the exact number of colored cells  
  that have to be adjacent (including diagonally) to it. Numbered cells  
  can contain the numbers {0, 1, 2} and cannot be colored.  
  [1] http://www2.stetson.edu/~efriedma/puzzle/latin/  
  ```  
  ; ┌────┬────┬────┬────┬────┐ ; ┌────┬────┬────┬────┬────┐ ;  
  ; │    │    │    │  2 │    │ ; │    │    │ clr│  2 │    │ ;  
  ; ├────┼────┼────┼────┼────┤ ; ├────┼────┼────┼────┼────┤ ;  
  ; │    │    │    │    │    │ ; │    │    │    │    │ clr│ ;  
  ; ├────┼────┼────┼────┼────┤ ; ├────┼────┼────┼────┼────┤ ;  
  ; │    │    │    │    │    │ ; │    │ clr│    │    │    │ ;  
  ; ├────┼────┼────┼────┼────┤ ; ├────┼────┼────┼────┼────┤ ;  
  ; │    │    │    │    │    │ ; │    │    │    │ clr│    │ ;  
  ; ├────┼────┼────┼────┼────┤ ; ├────┼────┼────┼────┼────┤ ;  
  ; │  1 │    │    │    │    │ ; │ clr│  1 │    │    │    │ ;  
  ; └────┴────┴────┴────┴────┘ ; └────┴────┴────┴────┴────┘ ;  
  ```  
  Figure 1: Example of a Latin Square Puzzle and its solution.  

  You should find propositional formulas which describe the puzzle and  
  which could be used to solve it. Focus on explaining the concept  
  of the formulas. You do not have to explicitly list all formulas and  
  you do not have to solve the puzzle.  
  Hints:
  Use propositional atoms C_ij, C_ij0 , C_ij1 ,C_ij2 to represent each cell of the (n×n) game board.
  If C_ij  has the value True, the cell "i, j" is colored, otherwise it is not colored.  
  If C_ijx has the value True, the cell "i, j" contains the number x.  
  Express the following constraints:  

  (a) There is exactly one colored cell in row i.  
  (b) No colored cells are adjacent to each other.  
  (c) No numbered cells can be colored.  
  (d) Numbered cells are adjacent to the indicated amount of colored cells.  

  ----------------------------------------------------------------------------  
  + Variables:
    - `C_ij`:  If the cell in row `i` and column `j` ist true, the cell is colored, otherwise uncolored.  
    - `C_ijx`: If the cell in row `i` and column `j` ist true, the cell contains the number `x`.  
  + (a) Row constraints:
    - Each row contains only one colored field.  
    - `C_0jx → ¬C_1jx ∧ ¬C_2jx ∧ ... ∧ ¬C_njx`  
  + Column constraints:
    - Each column contains only one colored field.  
    - `C_i0x → ¬C_i1x ∧ ¬C_i2x ∧ ... ∧ ¬C_inx`  
  + (b) Adjacent constraints:  
    - If a cell is colored, its adjacent cells contain are not colored.  
    - `C_{i+0,j+0} → ¬C_{i-1,j+1} ∧ ¬C_{i+0,j+1} ∧ ¬C_{i+1,j+1} ∧ ¬C_{i-1,j+0} ∧
      ¬C_{i+1,j+0} ∧ ¬C_{i-1,j-1} ∧ ¬C_{i+0,j-1} ∧ ¬C_{i+1,j-1}`  
    ```  
    ; ┌────┬────┬────┐ ;  
    ; │-1+1│+0+1│+1+1│ ;  
    ; ├────┼────┼────┤ ;  
    ; │-1+0│+0+0│+1+0│ ;  
    ; ├────┼────┼────┤ ;  
    ; │-1-1│+0-1│+1-1│ ;  
    ; └────┴────┴────┘ ;  
    ```  
  + (c) Numbered cell constraints:  
    - Cells containing numbers cannot be colored.  
    - `C_ijx → ¬C_ij`
  + (d) Adjacent indicator constraints:
    - Numbered cells are adjacent to the indicated amount of colored cells.  
    - x = {0, 1, 2}
    - `C_ij0 → (¬C_{i-1,j+1} ∧ ¬C_{i+0,j+1} ∧ ¬C_{i+1,j+1} ∧ ¬C_{i-1,j+0} ∧  
      ¬C_{i+1,j+0} ∧ ¬C_{i-1,j-1} ∧ ¬C_{i+0,j-1} ∧ ¬C_{i+1,j-1})`  
    - `C_ij1 → ( C_{i-1,j+1} ∧ ¬C_{i+0,j+1} ∧ ¬C_{i+1,j+1} ∧ ¬C_{i-1,j+0} ∧  
      ¬C_{i+1,j+0} ∧ ¬C_{i-1,j-1} ∧ ¬C_{i+0,j-1} ∧ ¬C_{i+1,j-1}) ∨  
      (power set of all combinations, where only one cell is not negated)`  
    - `C_ij2 → ( C_{i-1,j+1} ∧  C_{i+0,j+1} ∧ ¬C_{i+1,j+1} ∧ ¬C_{i-1,j+0} ∧  
      ¬C_{i+1,j+0} ∧ ¬C_{i-1,j-1} ∧ ¬C_{i+0,j-1} ∧ ¬C_{i+1,j-1}) ∨  
      (power set of all combinations, where only two cells are not negated)`  
    - All kind of logical combinations between `C_ij0` to `C_ij8` ...  
    - `C_ij8 → ( C_{i-1,j+1} ∧  C_{i+0,j+1} ∧  C_{i+1,j+1} ∧  C_{i-1,j+0} ∧  
      C_{i+1,j+0} ∧  C_{i-1,j-1} ∧  C_{i+0,j-1} ∧  C_{i+1,j-1})`  
  + Prefilled numbers:  
    - Row i=0, Column j=3, contains number x=2: `C_ijx = C_032`  
    - Row i=4, Column j=0, contains number x=1: `C_ijx = C_401`  



--------------------------------------------------------------------------------  
