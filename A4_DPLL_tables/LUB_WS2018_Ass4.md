# Logic and Computability WS18
## Assignment 4


--------------------------------------------------------------------------------
### Task 25 [2 Points]
In the DPLL algorithm you will need the resolution rule to
learn new clauses and to prove that a formula is not satisfiable.
Using natural deduction, prove the resolution rule:
`p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ`  


### Solution 25
+ Natural deduction:
  ```
  01. p ∨ φ         | premise
  02. ¬p ∨ ψ        | premise
  03. p ∨ ¬p        | LEM
  ----------------------------------------( Box1
  04. p             | assumption
  ----------------------------------------( Box3
  05. ¬p            | assumption
  06. ⊥             | ¬e 4,5
  07. ψ             | ⊥e 6
  ----------------------------------------) Box3
  ----------------------------------------( Box4
  08. ψ             | assumption
  ----------------------------------------) Box4
  09. ψ             | ∨e 2,5-7,8-8
  10. φ ∨ ψ         | ∨_2 9
  ----------------------------------------) Box1
  ----------------------------------------( Box2
  11. ¬p            | assumption
  ----------------------------------------( Box5
  12. p             | assumption
  13. ⊥             | ¬e 11,12
  14. φ             | ⊥e 13
  ----------------------------------------) Box5
  ----------------------------------------( Box6
  15. φ             | assumption
  ----------------------------------------) Box6
  16. φ             | ∨e 1,12-14,15-15
  17. φ ∨ ψ         | ∨i_1 16
  ----------------------------------------) Box2
  18. φ ∨ ψ         | ∨e 3,4-10,11-17
  ```



--------------------------------------------------------------------------------
### Task 26 [3+3+3+4 Points]
Use the DPLL algorithm (including Boolean Constraint Propagation (BCP),
pure literals, and conflict-driven clause learning) to check on paper,
if the following CNF formulas are satisfiable.

- Write down all the steps of the DPLL algorithm,
- draw the conflict graphs,
- and state the resolution proofs for all learned clauses.

If the formula is satisfiable, give a satisfying model,
else show a complete resolution proof for the formula’s unsatisfiability.

**Rules:**
- When resolving a conflict, only undo the last decision.
- Choose variables for decisions, BCP and pure literals in alphabetical order,
  starting with the negative phase (¬a > a > ¬b > b...).
- Always try to perform BCP first,
  before checking for pure literals,
  before making a decision.

**Hint:**
- BCP: Use BCP if there is only 1 Literal in clause: {l}
- PL:  Use PL if same literal occurs in all clauses in several clauses and BCP is not possible.
- DEC: Make a decision if neither BCP nor PL are possible.
- Conflict-graphs: If decision level is 0 than there is no conflict graph needed.
  However, for exercising reasons I still made them.



--------------------------------------------------------------------------------
### Task_26_a [3 Points]
+ Clauses:  
  C1 = {¬a,  c}  
  C2 = { a,  c}  
  C3 = {¬a, ¬b}  
  C4 = {¬b, ¬c}  
  C5 = { b, ¬c}  

| Step         | 1 | 2  | 3     | 4          | 5  | 6    | 7       |
| ------------ | - | -- | ----- | ---------- | -- | ---- | ------- |
| Decision lvl | 0 | 1  | 1     | 1          | 0  | 0    | 0       |
| Assignment   | - | ¬a | ¬a,c  | ¬a,c,¬b    | a  | a,¬b | a,¬b,¬c |
|              |   |    |       |            |    |      |         |
| C1: {¬a,c}   | 1 | ✔  | ✔     | ✔          | c  | c    | {}x     |
| C2: {a,c}    | 2 | c  | ✔     | ✔          | ✔  | ✔    | ✔       |
| C3: {¬a,¬b}  | 3 | ✔  | ✔     | ✔          | ¬b | ✔    | ✔       |
| C4: {¬b,¬c}  | 4 | 4  | ¬b    | ✔          | 4  | ✔    | ✔       |
| C5: {b,¬c}   | 5 | 5  | b     | {}x        | 5  | ¬c   | ✔       |
| C6: {a}      |   |    |       | Learned: a | ✔  | ✔    | ✔       |
|              |   |    |       |            |    |      |         |
| BCP          |   | c  | ¬b    | a          | ¬b | ¬c   | UNSAT   |
| Pure Literals|   |    |       |            |    |      |         |
| Decision     |¬a |    |       |            |    |      | .       |

+ Conflict graph 1:
  ```
  ;                    4         2
  ;   ⊥ <────── ¬b <──────  c <────── ¬a
  ;   Λ                     │
  ;   │                5    │
  ;   └────────  b <────────┘
  ;                 4
  ```
+ Resolution proof 1:
  - ad 4.: additional to step 4:
  ```
  C5: {b ∨ ¬c}, C4: {¬b ∨ ¬c}       | Apply rule : p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
          {¬c ∨ ε}, C2: {c ∨ a}     | Apply rule : ¬p ∨ ψ, p ∨ φ ⊢ φ ∨ ψ
                  a                 | Learned: {a}
  ```

+ Conflict graph 2:
  ```
  ;                    5         3
  ;   ⊥ <────── ¬c <────── ¬b <──────  a
  ;   Λ                     │          │
  ;   │                1    │    1     │
  ;   └────────  c <────────┘<─────────┘
  ```
+ Resolution proof 2:
- ad 7.: additional to step 7:
  ```
  C5: {c ∨ ¬a}, C1: {¬c ∨ b}        | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
          (b ∨ ¬a), C3: {¬b ∨ ¬a}   | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
                (¬a), C6: {a}       | Apply rule: φ ∧ ¬φ ⟹ ⊥
                    ⊥               | Result: UNSAT
  ```



--------------------------------------------------------------------------------
### Task_26_b [3 Points]
+ Clauses:  
  C1 = {¬a,  c}  
  C2 = { a,  b}  
  C3 = {¬a, ¬d}  
  C4 = { b, ¬c}  
  C5 = {¬b, ¬d}  
  C6 = {¬b,  d}  
  C7 = {¬c,  d}  

| Step          | 1  | 2  | 3    | 4          | 5  | 6   | 7     | 8        |
| ------------- | -- | -- | ---- | ---------- | -- | --- | ----- | -------- |
| Decision lvl  | 0  | 1  | 1    | 1          | 0  | 0   | 0     | 0        |
| Assignment    | -  | ¬a | ¬a,b | ¬a,b,¬d    | a  | a,c | a,b,c | a,b,c,¬d |
|               |    |    |      |            |    |     |       |          |
| 1: {¬a,c}     | 1  | ✔  | ✔    | ✔          | c  | ✔   | ✔     | ✔        |
| 2: {a,b}      | 2  | b  | ✔    | ✔          | ✔  | ✔   | ✔     | ✔        |
| 3: {¬a,¬d}    | 3  | ✔  | ✔    | ✔          | ¬d | ¬d  | ¬d    | ✔        |
| 4: {b,¬c}     | 4  | 4  | ✔    | ✔          | 4  | b   | ✔     | ✔        |
| 5: {¬b,¬d}    | 5  | 5  | ¬d   | ✔          | 5  | 5   | ¬d    | ✔        |
| 6: {¬b,d}     | 6  | 6  | d    | {}x        | 6  | 6   | d     | {}x      |
| 7: {¬c,d}     | 7  | 7  | 7    |            | 7  | d   | d     | {}x      |
| 8: {a}        |    |    |      | Learned: a | ✔  | ✔   | ✔     | ✔        |
|               |    |    |      |            |    |     |       |          |
| BCP           |    | b  | ¬d   | a          | c  | b   | ¬d    | UNSAT    |
| Pure Literals |    |    |      |            |    |     |       |          |
| Decision      | ¬a |    |      |            |    |     |       |          |

+ Conflict graph 1:
  ```
  ;                    5         2
  ;   ⊥ <────── ¬d <──────  b <────── ¬a
  ;   Λ                     │
  ;   │                6    │
  ;   └────────  d <────────┘
  ```
+ Resolution proof 1:
  - ad 4.: additional to step 4:
  ```
  C6: {d ∨ ¬b}, C5: {¬d ∨ ¬b}     | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
          (¬b ∨ ε), C2: {b ∨ a}   | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
                 a                | Learned: {a}
  ```

+ Conflict graph 2:
  ```
  ;                  3,5         4          1
  ;   ⊥ <────── ¬d <──────  b <──────  c <──────  a
  ;   Λ                     │
  ;   │              6,7    │
  ;   └────────  d <────────┘
  ```
+ Resolution proof 2:
  - ad 8.: additional to step 8:
  ```
  C5: {¬d ∨ ¬b}, C6: {d ∨ ¬b}           | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
           (¬b ∨ ε), C4: {b ∨ ¬c}       | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
               (¬c ∨ ε), C1: {c ∨ ¬a}   | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
                      (¬a), C8: {a}     | Apply rule: φ ∧ ¬φ ⟹ ⊥
                          ⊥             | Result: UNSAT
  ```


--------------------------------------------------------------------------------
### Task_26_c [3 Points]
+ Clauses:  
  C1 = {¬a,  b, ¬d}  
  C2 = {¬b,  c,  d}  
  C3 = { a,  b,  c}  
  C4 = {¬a,  c, ¬d}  
  C5 = { a, ¬b, ¬c}  
  C6 = { a,  b, ¬c}  
  C7 = { a, ¬b, ¬d}  
  C8 = {¬a, ¬b, ¬d}  

| Step          | 1 | 2     | 3     | 4        | 5     | 6    | 7       | 8          | 9     | 10    | 11     |
| ------------- | - | ----- | ----- | -------- | ----- | ---  | ------- | ---------- | ----- | ----- | ------ |
| Decision lvl  | 0 | 1     | 2     | 2        | 1     | 1    | 1       | 1          | 0     | 0     | 0      |
| Assignment    | - | ¬a    | ¬a,¬b | ¬a,¬b,¬c | ¬a    | ¬a,b | ¬a,b,¬c | ¬a,b,¬c,¬d | a     | a,c   | a,c,¬d |
| 1: {¬a,b,¬d}  | 1 | ✔     | ✔     | ✔        | ✔     | ✔    | ✔       | ✔          | b,¬d  | b,¬d  | ✔      |
| 2: {¬b,c,d}   | 2 | 2     | ✔     | ✔        | 2     | c,d  | d       | {}x        | 2     | ✔     | ✔      |
| 3: {a,b,c}    | 3 | b,c   | c     | {}x      | b,c   | ✔    | ✔       | ✔          | ✔     | ✔     | ✔      |
| 4: {¬a,c,¬d}  | 4 | ✔     | ✔     | ✔        | ✔     | ✔    | ✔       | ✔          | c,¬d  | ✔     | ✔      |
| 5: {a,¬b,¬c}  | 5 | ¬b,¬c | ✔     | ✔        | ¬b,¬c | ¬c   | ✔       | ✔          | ✔     | ✔     | ✔      |
| 6: {a,b,¬c}   | 6 | b,¬c  | ¬c    | ✔        | b,c   | ✔    | ✔       | ✔          | ✔     | ✔     | ✔      |
| 7: {a,¬b,¬d}  | 7 | ¬b,¬d | ✔     | ✔        | ¬b,¬d | ¬d   | ¬d      | ✔          | ✔     | ✔     | ✔      |
| 8: {¬a,¬b,¬d} | 8 | ✔     | ✔     | ✔        | ✔     | ✔    | ✔       | ✔          | ¬b,¬d | ¬b,¬d | ✔      |
| 9: {a,b}      |   |       |       | L: {a,b} | b     | ✔    | ✔       | ✔          | ✔     | ✔     | ✔      |
| 10: {a}       |   |       |       |          |       |      |         | L:{a}      | ✔     | ✔     | ✔      |
|               |   |       |       |          |       |      |         |            |       |       |        |
| BCP           |   |       | ¬c    |          | b     | ¬c   | ¬d      | a          |       |       |        |
| Pure Literals |   |       |       |          |       |      |         |            | c     | ¬d    | SAT    |
| Decision      |¬a | ¬b    |       | (¬a)     |       |      |         |            |       |       |        |

+ Conflict graph 1:
  ```
  ;              ┌─────────────────┐
  ;              V     6           │
  ;   ┌──────── ¬c <────────┐      │
  ;   V                     │      │
  ;   ⊥                    ¬b     ¬a
  ;   Λ                     │      │
  ;   └────────  c <────────┘      │
  ;              Λ     3           │
  ;              └─────────────────┘
  ```
  - Because there were two decisions made {¬a,¬b}, we learned {a,b}.
+ Resolution proof 1:
  - ad 4.: additional to step 4:
  ```
  C6: {¬c ∨ a ∨ b}, C3: {c ∨ a ∨ b}   | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
               (a ∨ b)                | Learned: {a ∨ b}
  ```

+ Conflict graph 2:
  ```
  ;                   7          5         9
  ;   ⊥ <────── ¬d <────── ¬c <──────  b <────── ¬a
  ;   Λ                     │
  ;   │               2     │
  ;   └────────  d <────────┘
  ```
  - Because there was one decisions made {¬a}, we learned {a}.
+ Resolution proof 2:
  - ad 8.: additional to step 8:
  ```
  C7: {¬d ∨ a ∨ ¬b}, C2: {d ∨ ¬b ∨ c}           | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
             (c ∨ a ∨ ¬b), C5: {¬c ∨ a ∨ ¬b}    | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
                      (¬b ∨ a), C9: {b ∨ a}     | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
                             (a)                | Learned: {a}

    ```

--------------------------------------------------------------------------------
### Task_26_d [4 Points]
+ Clauses:  
  C1 = { a,  c,  d}  
  C2 = {¬a, ¬b}  
  C3 = { b, ¬c}  
  C4 = {¬b, ¬e}  
  C5 = {¬c, ¬d}  
  C6 = { c,  e}  
  C7 = { c, ¬e}  

| Step          | 1  | 2   | 3     | 4        | 5           | 6   | 7    | 8       | 9         | 10           |
| ------------- | -- | --- | ----- | -------- | ----------- | --- | ---- | ------- | --------- | ------------ |
| Decision lvl  | 0  | 1   | 2     | 2        | 2           | 1   | 1    | 1       | 1         | 1            |
| Assignment    | -  | ¬a  | ¬a,¬b | ¬a,¬b,¬c | ¬a,¬b,¬c,¬e | ¬a  | ¬a,b | ¬a,b,¬e | ¬a,b,c,¬e | ¬a,b,c,¬d,¬e |
| 1: {a,c,d}    | 1  | c,d | c,d   | d        | d           | c,d | c,d  | c,d     | ✔         | ✔            |
| 2: {¬a,¬b}    | 2  | ✔   | ✔     | ✔        | ✔           | ✔   | ✔    | ✔       | ✔         | ✔            |
| 3: {b,¬c}     | 3  | 3   | ¬c    | ✔        | ✔           | 3   | ✔    | ✔       | ✔         | ✔            |
| 4: {¬b,¬e}    | 4  | 4   | ✔     | ✔        | ✔           | 4   | ¬e   | ✔       | ✔         | ✔            |
| 5: {¬c,¬d}    | 5  | 5   | 5     | ✔        | ✔           | 5   | 5    | 5       | ¬d        | ✔            |
| 6: {c,e}      | 6  | 6   | 6     | e        | {}x         | 6   | 6    | c       | ✔         | ✔            |
| 7: {c,¬e}     | 7  | 7   | 7     | ¬e       | ✔           | 7   | 7    | ✔       | ✔         | ✔            |
| 8: {b}        |    |     |       |          | L: {b}      | 8   | ✔    | ✔       | ✔         | ✔            |
|               |    |     |       |          |             |     |      |         |           |              |
| BCP           |    |     | ¬c    | ¬e       |             | b   | ¬e   | c       | ¬d        | SAT          |
| Pure Literals |    |     |       |          |             |     |      |         |           |              |
| Decision      | ¬a | ¬b  |       |          | (¬a)        |     |      |         |           |              |

+ Conflict graph 1:
  ```
  ;                              3
  ;                         ┌──────── ¬a
  ;                         │
  ;                   7     │    3
  ;   ⊥ <────── ¬e <────── ¬c <────── ¬b
  ;   Λ                     │
  ;   │               6     │
  ;   └──────── _e <────────┘
  ```
  - Because there were two decisions made {¬a,¬b}, we learned {a,b}.
+ Resolution proof 2:
  - ad 5.: additional to step 5:
  ```
  C6: {e ∨ c}, C7: {¬e ∨ c}       | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
          (c ∨ ε), C3: {¬c ∨ b}   | Apply rule: p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ
                 (b)              | Learned: {b}
  ```


--------------------------------------------------------------------------------



### Task_27 [Bonus: 2+2 Points]
Given are conflict graphs that arise during an execution of the
CDCL algorithm on some unspecified formulas. The conflict graphs
show the depth at which the variable was assigned inside the node
and the variable assignment itself is shown below the node. Decisions
are in this case nodes with no incoming edges and are coloured blue.
Incoming edges always come from assignments which imply the current
assignment. Conflicting nodes are nodes which assign the same variable
different values. These nodes are coloured red.

In the lecture, we learned only a single clause per conflict from the
conflict graph: the learned clause was obtained by negating all decisions
involved in the conflict. Modern SAT-Solvers are able to learn several
clauses from a single conflict in the following way:

- We find a cut of the graph so that the conflict is on the right side,
  and all decisions are on the left side;
- We take the partial assignment `ρ` made by all the variables assign-
  ments that have an outgoing edge crossing the cut;
- We add to the clause obtained by negating `ρ` to the clause database;

Following, we give an example. Given is a conflict graph. We make
two cuts, and from each cut we learn a new conflict clause.

![z_conflict_graph.png](z_conflict_graph.png)
