# Logic and Computability WS18
## Assignment 5

[@LUB_VIDEO_7](https://tube.tugraz.at/static/mh_default_org/engage-player/e6aae925-b864-4238-8827-3d2c294c51cc/37373439-8df2-4426-9963-1f6045be5703/presentation_38a29cd4_c6d9_4331_b936_38b0fead6762.mp4)  

--------------------------------------------------------------------------------
### Task 28 [1 Point]
The following table shows eight persons and whether they
live in the student dormitory or in their own flat:

| Name  | Housing   |
| ----- | --------- |
| Alice | Flat      |
| Bob   | Dormitory |
| Carl  | Flat      |
| David | Flat      |
| Eve   | Dormitory |
| Frank | Dormitory |
| Greg  | Dormitory |
| Hank  | Flat      |

Given the above set of persons, find a unique symbolic encoding
for each person and find a characteristic function for:
- (a) All persons that live in the dormitory.
- (b) All persons that live on their own.

### Solution 28

| Name  | Housing   | x2 x1 x0 |
| ----- | --------- | -------- |
| Alice | Flat      |  0  0  0 |
| Carl  | Flat      |  0  0  1 |
| David | Flat      |  0  1  0 |
| Hank  | Flat      |  0  1  1 |
| Bob   | Dormitory |  1  0  0 |
| Eve   | Dormitory |  1  0  1 |
| Frank | Dormitory |  1  1  0 |
| Greg  | Dormitory |  1  1  1 |

- (a) All persons that live in the dormitory: x2
- (b) All persons that live on their own: ¬x2



--------------------------------------------------------------------------------
### Task 29 [2 Points]
Find a symbolic binary encoding for `X = {0, 1, ..., 31}`.
Use it to find characteristic functions for the following sets.
If possible, simplify the formulas:

- a: `A = {0, 3, 4, 7}`
- b: `B = {x ∈ X | 28 ≤ x < 32}`
- c: `C = {21, 23, 29, 31}`
- d: `D = {x ∈ X | (4 ≤ x < 8), (12 ≤ x < 16), (20 ≤ x < 24)}`

### Solution 29
+ Base 2 to the power of 5 Bits: 2^5:
  - 32 different combinations
  - decimal number range from 0 to 31

### Solution 29_a
+ 29_a:
  - `A = {0, 3, 4, 7}`

| # | x4 x3 x2 x1 x0 | resulting literals          |
| - | -------------- | --------------------------- |
| 0 |  0  0  0  0  0 | ¬x4 ∧ ¬x3 ∧ ¬x2 ∧ ¬x1 ∧ ¬x0 |
| 3 |  0  0  0  1  1 | ¬x4 ∧ ¬x3 ∧ ¬x2 ∧  x1 ∧  x0 |
| 4 |  0  0  1  0  0 | ¬x4 ∧ ¬x3 ∧  x2 ∧ ¬x1 ∧ ¬x0 |
| 7 |  0  0  1  1  1 | ¬x4 ∧ ¬x3 ∧  x2 ∧  x1 ∧  x0 |

+ Symbolic binary encoding and simplification:  
  - `A = {0, 3, 4, 7}`
  - `A = {0 ∨ 3 ∨ 4 ∨ 7}`
  - Result: `A = {(¬x4) ∧ (¬x3) ∧ ((¬x0 ∧ ¬x1) ∨ (x0 ∧ x1))}`
  - Result: `A = {(¬x4) ∧ (¬x3) ∧ (x0 ↔ x1)}`



### Solution 29_b
+ 29_b:
  - `X = {0, 1, ..., 31}`
  - `B = {x ∈ X | 28 ≤ x < 32}`

| ## | x4 x3 x2 x1 x0 |
| -- | -------------- |
| 28 |  1  1  1  0  0 |
| 29 |  1  1  1  0  1 |
| 30 |  1  1  1  1  0 |
| 31 |  1  1  1  1  1 |

+ Symbolic binary encoding and simplification:  
  - `B = {x ∈ X | 28 ≤ x < 32}`
  - `B = {28, 29, 30, 31}`
  - `B = {(28) ∧ (29) ∧ (30) ∧ (31)}`
  - `B = {(x4) ∧ (x3) ∧ (x2)}`



### Solution 29_c
+ 29_c:
  - `C = {21, 23, 29, 31}`

| ## | x4 x3 x2 x1 x0 | resulting literals          |
| -- | -------------- | --------------------------- |
| 21 |  1  0  1  0  1 |  x4 ∧ ¬x3 ∧  x2 ∧ ¬x1 ∧  x0 |
| 23 |  1  0  1  1  1 |  x4 ∧ ¬x3 ∧  x2 ∧  x1 ∧  x0 |
| 29 |  1  1  1  0  1 |  x4 ∧  x3 ∧  x2 ∧ ¬x1 ∧  x0 |
| 31 |  1  1  1  1  1 |  x4 ∧  x3 ∧  x2 ∧  x1 ∧  x0 |

+ Symbolic binary encoding and simplification:  
  - `C = {21, 23, 29, 31}`
  - `C = {(21) ∧ (23) ∧ (29) ∧ (31)}`
  - `C = {(x0) ∧ (x2) ∧ (x4)}`



### Solution 29_d
+ 29_d:
  - `D = {x ∈ X | (4 ≤ x < 8), (12 ≤ x < 16), (20 ≤ x < 24)}`

| ## | 43210 | resulting literals          |
| -- | ----- | --------------------------- |
| 04 | 00100 | ¬x4 ∧ ¬x3 ∧  x2 ∧ ¬x1 ∧ ¬x0 |
| 05 | 00101 | ¬x4 ∧ ¬x3 ∧  x2 ∧ ¬x1 ∧  x0 |
| 06 | 00110 | ¬x4 ∧ ¬x3 ∧  x2 ∧  x1 ∧ ¬x0 |
| 07 | 00111 | ¬x4 ∧ ¬x3 ∧  x2 ∧  x1 ∧  x0 |
| 12 | 01100 | ¬x4 ∧  x3 ∧  x2 ∧ ¬x1 ∧ ¬x0 |
| 13 | 01101 | ¬x4 ∧  x3 ∧  x2 ∧ ¬x1 ∧  x0 |
| 14 | 01110 | ¬x4 ∧  x3 ∧  x2 ∧  x1 ∧ ¬x0 |
| 15 | 01111 | ¬x4 ∧  x3 ∧  x2 ∧  x1 ∧  x0 |
| 20 | 10100 |  x4 ∧ ¬x3 ∧  x2 ∧ ¬x1 ∧ ¬x0 |
| 21 | 10101 |  x4 ∧ ¬x3 ∧  x2 ∧ ¬x1 ∧  x0 |
| 22 | 10110 |  x4 ∧ ¬x3 ∧  x2 ∧  x1 ∧ ¬x0 |
| 23 | 10111 |  x4 ∧ ¬x3 ∧  x2 ∧  x1 ∧  x0 |

| ## | 4 3 2 1 0 | + |
| -- | --------- | - |
| 31 | 1 1 1 1 1 |   |
| 30 | 1 1 1 1 0 |   |
| 29 | 1 1 1 0 1 |   |
| 28 | 1 1 1 0 0 |   |
| 27 | 1 1 0 1 1 |   |
| 26 | 1 1 0 1 0 |   |
| 25 | 1 1 0 0 1 |   |
| 24 | 1 1 0 0 0 |   |
| 23 | 1 0 1 1 1 | + |
| 22 | 1 0 1 1 0 | + |
| 21 | 1 0 1 0 1 | + |
| 20 | 1 0 1 0 0 | + |
| 19 | 1 0 0 1 1 |   |
| 18 | 1 0 0 1 0 |   |
| 17 | 1 0 0 0 1 |   |
| 16 | 1 0 0 0 0 |   |
| 15 | 0 1 1 1 1 | + |
| 14 | 0 1 1 1 0 | + |
| 13 | 0 1 1 0 1 | + |
| 12 | 0 1 1 0 0 | + |
| 11 | 0 1 0 1 1 |   |
| 10 | 0 1 0 1 0 |   |
| 09 | 0 1 0 0 1 |   |
| 08 | 0 1 0 0 0 |   |
| 07 | 0 0 1 1 1 | + |
| 06 | 0 0 1 1 0 | + |
| 05 | 0 0 1 0 1 | + |
| 04 | 0 0 1 0 0 | + |
| 03 | 0 0 0 1 1 |   |
| 02 | 0 0 0 1 0 |   |
| 01 | 0 0 0 0 1 |   |
| 00 | 0 0 0 0 0 |   |

+ Symbolic binary encoding and simplification:  
  - `D = {x ∈ X | (4 ≤ x < 8), (12 ≤ x < 16), (20 ≤ x < 24)}`
  - `D = {23, 22, 21, 20, 15, 14, 13, 12, 07, 06, 05, 04}`
  - `D = {23 ∧ 22 ∧ 21 ∧ 20 ∧ 15 ∧ 14 ∧ 13 ∧ 12 ∧ 07 ∧ 06 ∧ 05 ∧ 04}`
  - `D = (x2) ∧ (¬x3 ∨ ¬x4)`



--------------------------------------------------------------------------------
### Task 30 [1+1 Points]
Find a symbolic encoding for the transition relation of
the following _Kripke_ structures and simplify your formulas.
The labels of the states symbolize their symbolic encoding.
+ Example: `10` = `x1 ∧ ¬x0`, i.e.:
  - the least significant bit corresponds to x0,
  - the second-least significant bit to x1 , and so forth.

![z_30.png](z_30.png)  



### Solution 30

+ Formalization of Digital System
  - Finite Set of States S: `S`
  - Set of Initial States I: `I ⊆ S`
  - Transition Relation T: `T ⊆ S × S`
  - Labeling Function L: `S → 2^AP`
    - Set of Atomic Propositions AP: `AP`

| a b | a ⊕ b | a ↔ b |
| --- | ----- | ----- |
| 0 0 |   0   |   1   |
| 0 1 |   1   |   0   |
| 1 0 |   1   |   0   |
| 1 1 |   0   |   1   |



### Solution 30_a
![z_30.png](z_30.png)  

| State Bitmask                 | Simplified       | Rewritten as Kripke encoding   |
| ----------------------------- | ---------------- | ------------------------------ |
| `(00) ∧ (01 ∨ 10 ∨ 11)'`      | `(00) ∧ ¬(00)'`  | `(¬x1 ∧ ¬x0) ∧ ¬(¬x1' ∧ ¬x0')` |
| `(01) ∧ (00 ∨ 01 ∨ 10 ∨ 11)'` | `(01) ∧  (⊤)'`   | `(¬x1 ∧  x0) ∧  (⊤)'`          |
| `(10) ∧ (00 ∨ 11)'`           | `(10) ∧  (1↔1)'` | `( x1 ∧ ¬x0) ∧  ( x1' ↔ x0')`  |
| `(11) ∧ (01 ∨ 10 ∨ 11)'`      | `(11) ∧ ¬(00)'`  | `( x1 ∧  x0) ∧ ¬(¬x1' ∧ ¬x0')`  |

From the table above we can derive following Kripke encoding:
```
(¬x1 ∧ ¬x0) ∧ ¬(¬x1' ∧ ¬x0') ∨
(¬x1 ∧  x0) ∨
( x1 ∧ ¬x0) ∧  ( x1' ↔ x0') ∨
( x1 ∧  x0) ∧ ¬(¬x1' ∧ x0')
```
Simplifying line 1 and 4 with biconditional `↔`:  
```
(¬x1 ∧  x0) ∨
( x1 ∧ ¬x0) ∧  ( x1' ↔ x0') ∨
( x1 ↔  x0) ∧ ¬(¬x1' ∧ x0')
```



### Solution 30_b
![z_30.png](z_30.png)  

| State Bitmask       | Simplified      | Rewritten as Kripke encoding  |
| ------------------- | --------------- | ----------------------------- |
| `(00) ∧ (00 ∨ 01)'` | `(00) ∧ (0_)'`  | `(¬x1 ∧ ¬x0) ∧ (¬x1')`        |
| `(01) ∧ (11)'`      | `(01) ∧ (11)'`  | `(¬x1 ∧  x0) ∧ ( x1' ∧  x0')` |
| `(10) ∧ (01)'`      | `(10) ∧ (01)'`  | `( x1 ∧ ¬x0) ∧ (¬x1' ∧  x0')` |
| `(11) ∧ (00 ∨ 11)'` | `(11) ∧ (1↔1)'` | `( x1 ∧  x0) ∧ ( x1' ↔  x0')` |

From the table above we can derive following Kripke encoding:
```
(¬x1 ∧ ¬x0) ∧ (¬x1') ∨
(¬x1 ∧  x0) ∧ ( x1' ∧  x0') ∨
( x1 ∧ ¬x0) ∧ (¬x1' ∧  x0') ∨
( x1 ∧  x0) ∧ (x1' ↔ x0')
```



--------------------------------------------------------------------------------
### Task 31 [2 Points]
Build a Kripke structure from the following symbolically encoded
transition relations and draw the corresponding graph:
```
Line_1:  ( x0 ⊕  x1) ∧ ¬(¬x0' ∨ ¬x1') ∨
Line_2: ¬( x0 →  x1) ∧  (¬x0'       ) ∨
Line_3:  ( x0 ↔  x1) ∧ ¬( x0' ⊕  x1')
```



### Solution 31
Rewriting Literals:  
```
Line_1:  ( x1 ⊕  x0) ∧ ¬(¬x1' ∨ ¬x0') ∨
Line_2: ¬( x1 ←  x0) ∧  (       ¬x0') ∨
Line_3:  ( x1 ↔  x0) ∧ ¬( x1' ⊕  x0')
```

Dissolve individual formulas:
```
Line_1:
  (x1 ⊕ x0) ∧ ¬(¬x1' ∨ ¬x0') ∨
  ((¬x1 ∧ x0) ∨ (x1 ∧ ¬x0)) ∧ (x1' ∨ x0') ∨
  (01 ∨ 10) ∧ (11)

Line_2:
  ¬(x1 ← x0) ∧ (¬x0') ∨
  ¬(x1 ∨ ¬x0) ∧ (¬x0') ∨
  (¬x1 ∧ x0) ∧ (¬x0') ∨
  (01) ∧ (00 ∨ 10)

Line_3:
  (x1 ↔ x0) ∧ ¬(x1' ⊕ x0')
  (x1 ↔ x0) ∧ (x1' ↔ x0')
  ((x1 ∧ x0) ∨ (¬x1 ∧ ¬x0)) ∧ ((x1' ∧ x0') ∨ (¬x1' ∧ ¬x0'))
  (11 ∨ 00) ∧ (11 ∨ 00)
```


![z_31.png](z_31.png)




--------------------------------------------------------------------------------
### Task 32 [1 Point]
Use the BDD shown in the figure on the right to
check if the function it represents evaluates to true
or false with the following variable assignments.  
- 32a: `a=⊤, b=⊤, c=⊥, d=⊤`  
- 32b: `a=⊥, b=⊤, c=⊤, d=⊥`  


![z_32_task.png](z_32_task.png)


### Solution 32
- 32a: Green: `a=1, b=1, c=0, d=1`  
- 32b: Blue:  `a=0, b=1, c=1, d=0`  


![z_32_solution.png](z_32_solution.png)


+ 32a:
  - Green: `Model Ma = {a=1, b=1, c=0, d=1} = ⊥`  
  - M = {a ∧ b ∧ ¬c} = ⊥
+ 32b:
  - Blue:  `Model Mb = {a=0, b=1, c=1, d=0} = ⊥`  
  - Mb = {¬a ∧ b ∧ c ∧ ¬d} = ⊥



--------------------------------------------------------------------------------
### Task 33 [1 Point]
Find a propositional formula for the function `f` that is
represented by the BDD from the previous example.


### Solution 33
+ Distributivgesetze:
  - (x ∧ y) ∨ (x ∧ z) = x ∧ (y ∨ z)
  - (x ∨ y) ∧ (x ∨ z) = x ∨ (y ∧ z)


```
f = (a ∧ b ∧ c) ∨ (a ∧ b ∧ ¬c) ∨ (a ∧ ¬b) ∨ (¬a ∧ b ∧ c ∧ d) ∨ (¬a ∧ b ∧ c ∧ ¬d)
f = (a ∧ b) ∨ (a ∧ ¬b) ∨ (¬a ∧ b ∧ c)
f = (a) ∨ (¬a ∧ b ∧ c)
f = (b ∧ c)
```





--------------------------------------------------------------------------------
### Task 34 [1 Point]
Convert the following BDD into a reduced and
ordered BDD with variable order `a < b < c`.


![z_34_task.png](z_34_task.png)


### Solution 34
![z_34_solution.png](z_34_solution.png)



--------------------------------------------------------------------------------
### Task 35 [2+3 Points]
In this task you will have to construct reduced and ordered BDDs.  
Use complemented edges and a node for `⊤` as the only constant node.  
To simplify drawing, you may assume that "dangling" edges point to the constant node.  
Write down all cofactors and mark them in the graph that you draw.  

+ (a) [2 Points]
  - `f = (a ↔ b) ∧ (a ⊕ c)`
  - Use the following variable order: `a < c < b`.


+ (b) [3 Points]
  - `f = (a ∧ b ∧ c) ∨ (b ∧ ¬d) ∨ (d ∧ (c → a))`
  - Use the following variable order: `b < a < c < d`.



### Solution 35_a
- Order: `a < c < b`
- Function: `f = (a ↔ b) ∧ (a ⊕ c)`
  ```
  00 | f = (a ↔ b) ∧ (a ⊕ c)
  00 | f = ((a ∧ b) ∨ (¬a ∧ ¬b)) ∧ ((a ∧ ¬c) ∨ (¬a ∧ c))
  01 | f_a = b ∧ ¬c
  02 | f_ac = ⊥
  03 | f_a¬c = b
  04 | f_a¬cb = ⊤
  05 | f_a¬c¬b = ⊥
  06 | f_¬a = ¬b ∧ c
  07 | f_¬ac = ¬b == ¬f_a¬c
  08 | f_¬a¬c = ⊥
  ```
  ROBDD - Reduced Order Binary Decision Diagram:  
  ![z_35_a.png](z_35_a.png)



### Solution 35_b
- Order: `b < a < c < d`
- Function: `f = (a ∧ b ∧ c) ∨ (b ∧ ¬d) ∨ (d ∧ (c → a))`
  ```
  L0: f = (a ∧ b ∧ c) ∨ (b ∧ ¬d) ∨ (d ∧ (c → a))
  L0: f = (a ∧ b ∧ c) ∨ (b ∧ ¬d) ∨ (d ∧ (¬c ∨ a))
  L1: f_b = (a ∧ c) ∨ (¬d) ∨ (d ∧ (¬c ∨ a))
  L2: f_ba = ⊤
  L3: f_b¬a = (¬d) ∨ (d ∧ ¬c)
  L4: f_b¬ac = ¬d
  L5: f_b¬acd = ⊥
  L6: f_b¬ac¬d = ⊤
  L7: f_b¬a¬c = ⊤
  L8: f_¬b = d ∧ (¬c ∨ a)
  L9: f_¬ba = d == ¬f_b¬ac == ¬¬d
  L10: f_¬b¬a = d ∧ ¬c
  L11: f_¬b¬ac = ⊥
  L12: f_¬b¬a¬c = d == ¬f_b¬ac == ¬¬d

  L10: f_¬bad = ⊤
  L11: f_¬ba¬d = ⊥
  L12: f_¬b¬a = d ∧ ¬c
  L13: f_¬b¬ac = ⊥
  L14: f_¬b¬a¬c = d
  L15: f_¬b¬a¬cd = ⊤
  L16: f_¬b¬a¬c¬d = ⊥
  ```
  ROBDD - Reduced Order Binary Decision Diagram:  
  ![z_35_b.png](z_35_b.png)


--------------------------------------------------------------------------------
