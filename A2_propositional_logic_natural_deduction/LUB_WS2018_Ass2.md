-------------------------------------------------------------------------------
# Logic and Computability WS18, Assignment 2
For each of the following sequences, either provide a natural deduction
proof, or a counter-example that proves the sequent invalid.
For proofs, clearly indicate which rule, and what assumptions/
premises/intermediate results you are using in each step.
Also clearly indicate the scope of any boxes you use.
For counterexamples, give a complete model. Show that the model
satisfies the premise(s) of the sequent in question, but does not
satisfy the respective conclusion.



--------------------------------------------------------------------------------
### LUB_2018_Assignment_2
+ d Task_05:   `¬q ⊢ q → p`
+ d Task_06:   `p → ¬p ⊢ ¬p`
+ C Task_07:   `¬q ⊢ p → q`
+ d Task_08:   `¬¬¬p, ¬¬p ⊢ s`
+ d Task_09_a: `t → s ⊢ ¬s → ¬t`
+ d Task_09_b: `¬s → ¬t ⊢ t → s`
+ d Task_10:   `¬(¬p ∨ q) ⊢ p`
+ d Task_11:   `q ∧ p, ¬¬p → r ⊢ r ∧ q`
+ d Task_12:   `⊢ q → (p ∨ q)`
+ d Task_13:   `(p ∧ q) ∧ r ⊢ (r ∧ q) ∧ p`
+ C Task_14_a: `(¬q ∧ p) → (q ∧ r) ⊢ p`
+ C Task_14_b: `¬q, p → (q ∧ r) ⊢ p`
+ d Task_15_a: `¬(p ∧ q), p ⊢ ¬q`
+ C Task_15_b: `p ∨ q, p ⊢ ¬q`
- ? Task_16:   `(p → r) ∧ (q → s) ⊢ (p ∧ q) → (r ∧ s)`
- ? Task_17:   `p → (q ∨ r), q → s, r → s ⊢ p → s`
- ? Task_18:   `⊢ (p → q) ∨ (q → r)`


### LUB_2018_Assignment_2_Task_1_to_4
- Task 1 to 4 are in assignment 1.


### LUB_2018_Assignment_2_Task_05
- Sequence: `¬q ⊢ q → p`
- Contradiction: NO:
  ```
  M = {q=0, p=1}
  ¬q ⊢  q → p
  ¬q ⊢ ¬q ∨ p
  ¬0 ⊢ ¬0 ∨ 1
   1 ⊢ 1
  ```
- Deduction:
  ```
  1. ¬q           | premise
  ----------------------------------------( Box1
  2. q            | assumption
  3. ¬q           | copy 1
  4. ⊥            | ¬e 2,3
  5. p            | ⊥e 4
  ----------------------------------------) Box1
  6. q → p        | →i 2-5
  ψ= q → p        | conclusion
  ```


### LUB_2018_Assignment_2_Task_06
- Sequence: `p → ¬p ⊢ ¬p`
- Contradiction: NO:
  ```
  M = {p=1}
   p → ¬p ⊢ ¬p
  ¬p ∨ ¬p ⊢ ¬p
  ¬0 ∨ ¬0 ⊢ ¬0
   1 ∨  1 ⊢  1
        1 ⊢  1
  ```
- Deduction:
  ```
  1. p → ¬p       | premise
  ----------------------------------------( Box1
  2. p            | assumption
  3. ¬p           | →e 1,2
  4. ⊥            | ¬e 2,3
  ----------------------------------------) Box1
  0. ¬p           | ¬i, 2-4
  ψ= ¬p           | conclusion
  ```



### LUB_2018_Assignment_2_Task_07
- Sequence: `¬q ⊢ p → q`
- Contradiction: YES:
  ```
  M = {q=0, p=1}
  ¬q ⊢  p → q
  ¬q ⊢ ¬p ∨ q
  ¬0 ⊢ ¬1 ∨ 0
   1 ⊢  0 ∨ 0
   1 ⊢ 0
  ```
- Deduction: None, because contradiction!



### LUB_2018_Assignment_2_Task_08
- Sequence: `¬¬¬p, ¬¬p ⊢ s`
- Contradiction: -----------------------------------------------< Contradiction?
  ```
  M = {p=1, s=0}

  Premise 1:
  ¬¬¬p ⊢ s
    ¬p ⊢ s
    ¬1 ⊢ 0
     0 ⊢ 0

  Premise 2:
   ¬¬p ⊢ s
     p ⊢ s
     1 ⊢ 0
  ```
- Deduction:
  ```
  1. ¬¬¬p   | premise
  2. ¬¬p    | premise
  3. ¬p     | ¬¬e 1
  4. p      | ¬¬e 2
  5. ⊥      | ¬e 1,2
  6. s      | ⊥e 5
  ψ= s      | conclusion
  ```



### LUB_2018_Assignment_2_Task_09_a
- Sequence: `t → s ⊢ ¬s → ¬t`
- Contradiction: NO:
  ```
  M = {s=0, t=0}
   t → s ⊢ ¬s → ¬t
  ¬t ∨ s ⊢  s ∨ ¬t
  ¬t ∨ s ⊢ ¬t ∨  s
  ¬0 ∨ 0 ⊢ ¬0 ∨  0
   1 ∨ 0 ⊢  1 ∨  0
       1 ⊢ 1
  ```
- Deduction:
  ```
  1. t → s        | premise
  ----------------------------------------( Box1
  2. ¬s           | assumption
  3. ¬t           | MT 1,2
  ----------------------------------------) Box1
  4. ¬s → ¬t      | →i
  ψ= ¬s → ¬t      | conclusion
  ```



### LUB_2018_Assignment_2_Task_09_b
- Sequence: `¬s → ¬t ⊢ t → s`
- Contradiction: NO:
  ```
   M = {s=0, t=0}
   ¬s → ¬t ⊢  t →  s
    s ∨ ¬t ⊢ ¬t ∨  s
    s ∨ ¬t ⊢  s ∨ ¬t
    0 ∨ ¬0 ⊢  0 ∨ ¬0
    0 ∨ ¬1 ⊢  0 ∨ ¬1
         1 ⊢ 1
  ```
- Deduction:
  ```
  1. ¬s → ¬t      | premise
  ----------------------------------------( Box1
  2. t            | assumption
  3. ¬¬t          | ¬¬i 2
  4. ¬¬s          | MT 1,3
  5. s            | ¬¬e 3
  ----------------------------------------) Box1
  6. t → s        | →i n-n
  ψ= t → s        | conclusion
  ```



### LUB_2018_Assignment_2_Task_10
- Sequence: `¬(¬p ∨ q) ⊢ p`
- Contradiction: NO:
  ```
  M = {p=0, q=0}
  ¬(¬p ∨  q) ⊢ p
   ( p ∧ ¬q) ⊢ p
   ( 0 ∧ ¬1) ⊢ 0
   ( 0 ∧  0) ⊢ 0
           0 ⊢ 0
  ```
- Deduction:
  ```
  1. ¬(¬p ∨ q)    | premise
  ----------------------------------------( Box1
  2. ¬p           | assumption
  3. ¬p ∨ q       | ∨i_1 2
  4. ⊥            | ¬e 3,1
  ----------------------------------------) Box1
  5. ¬¬p          | ¬i 2-4
  6. p            | ¬¬e 5
  ψ= p            | conclusion
  ```



### LUB_2018_Assignment_2_Task_11
- Sequence: `q ∧ p, ¬¬p → r ⊢ r ∧ q`
- Contradiction: -----------------------------------------------< Contradiction?
  ```
  M = {p=1, q=1, r=0}

  Premise 1:
  q ∧ p ⊢ r ∧ q
  1 ∧ 1 ⊢ 0 ∧ 1
      1 ⊢ 0

  Premise 2:
  ¬¬p → r ⊢ r ∧ q
    p → r ⊢ r ∧ q
   ¬p ∨ r ⊢ r ∧ q
   ¬1 ∨ 0 ⊢ 0 ∧ 1
    0 ∨ 0 ⊢ 0 ∧ 1
        0 ⊢ 0
  ```
- Deduction:
  ```
  1. q ∧ p        | premise
  2. ¬¬p → r      | premise
  3. p            | ∧e_2 1
  4. ¬¬p          | ¬¬i 2
  5. r            | →e 4,2
  6. q            | ∧e_1 1
  7. r ∧ q        | ∧1 5,6
  ψ= r ∧ q        | conclusion
  ```


### LUB_2018_Assignment_2_Task_12
- Sequence: `⊢ q → (p ∨ q)`
- Contradiction: -----------------------------------------------< Contradiction?
- Deduction:
  ```
  ----------------------------------------( Box1
  1. q            | assumption
  2. p ∨ q        | ∧i_2 1
  ----------------------------------------) Box1
  3. q → (p ∨ q)  | →i 1-2 (Box1)
  ψ= q → (p ∨ q)  | conclusion
  ```



### LUB_2018_Assignment_2_Task_13
- Sequence: `(p ∧ q) ∧ r ⊢ (r ∧ q) ∧ p`
- Contradiction: NO:
  ```
  M = {p=1, q=1, r=1}
  (p ∧ q) ∧ r ⊢ (r ∧ q) ∧ p
  (p ∧ q) ∧ r ⊢ (r ∧ q) ∧ p
  (1 ∧ 1) ∧ 1 ⊢ (1 ∧ 1) ∧ 1
            1 ⊢ 1
  ```
- Deduction:
  ```
  1. (p ∧ q) ∧ r  | premise
  2. p ∧ q        | ∧e_1 1
  3. p            | ∧e_1 2
  4. q            | ∧e_2 2
  5. r            | ∧e_2 1
  6. r ∧ q        | ∧i 5,4
  7. (r ∧ q) ∧ p  | ∧i 6,3
  ψ= (r ∧ q) ∧ p  | conclusion
  ```



### LUB_2018_Assignment_2_Task_14_a
- Sequence: `(¬q ∧ p) → (q ∧ r) ⊢ p`
- Contradiction: YES:
  ```
  M = {p=0, q=0, r=0}
   (¬q ∧  p) → (q ∧ r) ⊢ p
  ¬(¬q ∧  p) ∨ (q ∧ r) ⊢ p
   ( q ∨ ¬p) ∨ (q ∧ r) ⊢ p
   ( 0 ∨ ¬0) ∨ (0 ∧ 0) ⊢ 0
   ( 0 ∨  1) ∨ (0 ∧ 0) ⊢ 0
   (   1   ) ∨ (  0  ) ⊢ 0
                     1 ⊢ 0
  ```
- Deduction: None, because contradiction!



### LUB_2018_Assignment_2_Task_14_b
- Sequence: `¬q, p → (q ∧ r) ⊢ p`
- Contradiction: YES:
  ```
  M = {p=0, q=1, r=1}

  ¬q ⊢ p
  ¬1 ⊢ 0
   0 ⊢ 0

   p → (q ∧ r) ⊢ p
  ¬p ∨ (q ∧ r) ⊢ p
  ¬0 ∨ (1 ∧ 1) ⊢ 0
   1 ∨ (1 ∧ 1) ⊢ 0
             1 ⊢ 0
  ```
- Deduction: None, because contradiction!



### LUB_2018_Assignment_2_Task_15_a
- Sequence: `¬(p ∧ q), p ⊢ ¬q`
- Contradiction: NO:
  ```
  M = {p=1, q=0}

  ¬(p ∧  q) ⊢ ¬q
   ¬p ∨ ¬q  ⊢ ¬q
   ¬1 ∨ ¬0  ⊢ ¬0
    0 ∨  1  ⊢  1
          1 ⊢ 1

  p ⊢ ¬q
  1 ⊢ ¬0
  1 ⊢ 1
  ```
- Deduction:
  ```
  1. ¬(p ∧ q)     | premise
  2. p            | premise
  ----------------------------------------( Box1
  3. q            | assumption
  4. p ∧ q        | ∧i 2,3
  6. ⊥            | ¬e 4,1
  ----------------------------------------) Box1
  8. ¬q           | ¬e 3-6
  ψ= ¬q           | conclusion
  ```



### LUB_2018_Assignment_2_Task_15_b
- Sequence: `p ∨ q, p ⊢ ¬q`
- Contradiction: YES:
  ```
  M = {p=1, q=1}

  p ∨ q ⊢ ¬q
  1 ∨ 1 ⊢ ¬1
  1 ∨ 1 ⊢  0
      1 ⊢  0

  p ⊢ ¬q
  1 ⊢ ¬0
  1 ⊢  1
  ```
- Deduction: None, because contradiction!



### LUB_2018_Assignment_2_Task_16
- Sequence: `(p → r) ∧ (q → s) ⊢ (p ∧ q) → (r ∧ s)`
- Contradiction: NO:
  ```
  M = {p=1, q=1, r=1, s=1}
  (p → r) ∧ (q → s) ⊢ (p ∧ q) → (r ∧ s)
  (¬p ∨ r) ∧ (¬q ∨ s) ⊢ ¬(p ∧ q) ∨ (r ∧ s)
  (¬p ∨ r) ∧ (¬q ∨ s) ⊢ (¬p ∨ ¬q) ∨ (r ∧ s)
  (¬1 ∨ 1) ∧ (¬1 ∨ 1) ⊢ (¬1 ∨ ¬1) ∨ (1 ∧ 1)
  ( 0 ∨ 1) ∧ ( 0 ∨ 1) ⊢ ( 0 ∨  0) ∨ (1 ∧ 1)
  (   1  ) ∧ (   1  ) ⊢ (   0   ) ∨ (  1  )
                    1 ⊢ 1
  ```
- Deduction:
  ```
  01. (p → r) ∧ (q → s) | premise
  02. p → r       | ∧e_1 1
  03. q → s       | ∧e_2 1
  ----------------------------------------( Box1
  04. p ∧ q       | assumption
  05. p           | ∧e_1 4
  06. q           | ∧e_2 4
  07. r           | →e 5,2
  08. s           | →e 6,3
  09. r ∧ s       | ∧i 7,8
  ----------------------------------------) Box1
  10. (p ∧ q) → (r ∧ s) | →i 4,9
  ψ = (p ∧ q) → (r ∧ s) | conclusion
  ```



### LUB_2018_Assignment_2_Task_17
- Sequence: `p → (q ∨ r), q → s, r → s ⊢ p → s`
- Deduction:
  ```
  01. p → (q ∨ r) | premise
  02. q → s       | premise
  03. r → s       | premise
  ----------------------------------------( Box1
  04. p           | assumption
  05. q ∨ r       | →e 4,1
  ----------------------------------------( Box2
  06. q           | assumption
  07. s           | →e 6,2
  ----------------------------------------) Box2
  ----------------------------------------( Box3
  08. r           | assumption
  09. s           | →e 8,3
  ----------------------------------------) Box3
  10. s           | ∨e 5, 6-7, 8-9
  ----------------------------------------) Box1
  11. p → s       | →i 4-10
  ψ = p → s       | conclusion
  ```



### LUB_2018_Assignment_2_Task_18
- Sequence: `⊢ (p → q) ∨ (q → r)`
- Contradiction: NO:
  ```
  ⊢ (p → q) ∨ (q → r)
  ⊢ ¬p ∨  q ∨ ¬q  ∨ r
  ⊢ ¬p ∨ (q ∨ ¬q) ∨ r
  ⊢ ¬p ∨ ⊥ ∨ r
  ⊢ ¬p ∨ r
  ```
- Deduction:
  ```
  01. q ∨ ¬q      | LEM
  ----------------------------------------( Box1
  02. q           | assumption
  ----------------------------------------( Box3
  03. p           | assumption
  04. q           | copy
  ----------------------------------------) Box3
  05. p → q       | →i 3,4
  06. (p → q) ∨ (q → r) | ∨i 5
  ----------------------------------------) Box1
  ----------------------------------------( Box2
  07. ¬q          | assumption
  ----------------------------------------( Box4
  08. q           | assumption
  09. ⊥           | PBC 7,8
  10. r           | ¬e 9
  ----------------------------------------) Box4
  11. q → r       | →i 8-10
  12. (p → q) ∨ (q → r) | ∨i 11
  ----------------------------------------) Box2
  12. (p → q) ∨ (q → r) | ∨e 1, 2-6, 7-12
  ψ = (p → q) ∨ (q → r) | conclusion
  ```
