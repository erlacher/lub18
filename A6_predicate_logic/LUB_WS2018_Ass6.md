# Logic and Computability WS18
## Assignment 6

[@LUB_WS18_VIDEOs](https://tube.tugraz.at/paella/ui/browse.html?series=5a8d61d8-3e4b-4298-b3c5-f791d58a06ed&category=courses)
[@LUB_WS18_VIDEO_7]()


--------------------------------------------------------------------------------
### Task 36 [3 Points]
Translate the following sentences into predicate logic.
Be as precise as possible. Give the meaning of any
function and predicate symbols you use.
- (a) Every person needs a purpose.
- (b) Not everybody likes everybody.
- (c) Everybody loves my baby, but my baby loves nobody but me.


### Solution 36
+ (a) Every person needs a purpose.
  ```
  Person(x).    % x is a person.
  Purpose(y).   % y is a purpose.
  Needs(x,y).   % x needs a purpose y.
  ∀x, ∃y: (Person(x) ∧ Purpose(y) → Needs(x,y))
  ```


+ (b) Not everybody likes everybody.
  ```
  P(x).     % x is a person.
  P(y).     % y is a person.
  L(x,y).   % x likes y.
  ¬∀x, ∀y: (P(x) ∧ P(y) ∧ Likes(x,y))
  ```


+ (c) Everybody loves my baby, but my baby loves nobody but me.
  ```
  P(x).     % x is a person.
  B(y).     % y is a baby.
  M(z).     % z is me.
  Par(z,y). % z is parent of y.
  L(x,y).   % x loves y.
  L(y,z).   % y loves z.
  ∃!y, ∀x, ∃!z: ((P(x) ∧ B(y) ∧ M(z) ∧ Par(z,y)) → (L(x,y) ∧ L(y,z) ∧ ¬L(y,x)))
  ```



--------------------------------------------------------------------------------
### Task 37 [Bonus: 1 Point]
  The last sentence of the last exercise originates from
  a popular 1930s American song. It also includes a perhaps surprising
  statement about the relationship between the singer and his/her baby.
  Find it by drawing logical conclusions.

### Solution 37
  - Proposition: "Everybody loves my baby, but my baby loves nobody but me."
  - Because *everybody* loves my baby, it means that the baby also loves itself.
  - The baby however loves *nobody* but me.
  - Therefore we can conclude that I have to be the baby.
  ```
  P(p).     % p is a person.
  P(m).     % m is a person.
  P(b).     % b is a person.
  M(m).     % m is me.
  B(b).     % b is baby.
  L(p,b).   % p loves b.
  L(b,m).   % b loves m.
  I(b,m).   % b is_same_person_as m.
  ∀p, ∃!m, ∃!b: P(p) ∧ P(m) ∧ P(b) ∧ M(m) ∧ B(b) ∧ L(p,b) ∧ L(b,m) → I(b,m).
  ```



--------------------------------------------------------------------------------
### Task 38_a [1 Point]
Consider the sentence `φ = ∃x, ∀y: (P(y,x) ∧ (P(x,y) → (Q(x,y) ∨ R(x,y))))`.  
Do the following models satisfy `φ`?  

+ (a) The model `M` consists of:
  - Domain-set A: `A = {a, b, c}`
  - Predicate P:  `P^M = {(a,b), (b,c), (c,a)}`
  - Predicate Q:  `Q^M = {(a,b), (a,c), (b,a), (c,c)}`
  - Predicate R:  `R^M = {(a,m) | m ∈ A}`

### Solution 38_a
  ```
  Applying De Morgan's Law:
  φ = ∃x, ∀y: (P(y,x) ∧ ( P(x,y) → (Q(x,y) ∨ R(x,y))))
  φ = ∃x, ∀y: (P(y,x) ∧ (¬P(x,y) ∨ (Q(x,y) ∨ R(x,y))))

  Applying domain-set on formula: x=a, y=a:
  φ = ∃a, ∀a: (P(a,a) ∧ (¬P(a,a) ∨ (Q(a,a) ∨ R(a,a))))
  φ = ∃a, ∀a: (⊥      ∧ (⊤       ∨ (⊥      ∨ ⊥     ))) = ⊥ ... UNSAT

  Applying domain-set on formula: x=a, y=b:
  φ = ∃a, ∀b: (P(b,a) ∧ (¬P(a,b) ∨ (Q(a,b) ∨ R(a,b))))
  φ = ∃a, ∀b: (⊥      ∧ (⊥       ∨ (⊤      ∨ ⊤     ))) = ⊥ ... UNSAT

  Applying domain-set on formula: x=a, y=c:
  φ = ∃a, ∀c: (P(c,a) ∧ (¬P(a,c) ∨ (Q(a,c) ∨ R(a,c))))
  φ = ∃a, ∀c: (⊤      ∧ (⊤       ∨ (⊤      ∨ ⊤     ))) = ⊤ ... SAT

  M satisfies φ, iff for all possible variable assignments on y,
  each time the formula evaluates to true.
  In case of "x=a, y=a" and "x=a, y=b", this does not happen, therefore:
  M ⊭ φ
  ```



--------------------------------------------------------------------------------
### Task 38_b [1 Point]
Consider the sentence `φ = ∃x ∀y (P(y,x) ∧ (P(x,y) → (Q(x,y) ∨ R(x,y))))`.  
Do the following models satisfy `φ`?  


+ (b) The model `M'` consists of:
  - Domain-set A: `A = ℕ`
  - Predicate P:  `P^M' = {(m,n) | m ≥ n}`
  - Predicate Q:  `Q^M' = {(m,n) | m = 2n}`
  - Predicate R:  `R^M' = {(m,n) | m < n}`


### Solution 38_b
  ```
  Applying De Morgan's Law:
  φ = ∃x ∀y (P(y,x) ∧ ( P(x,y) → (Q(x,y) ∨ R(x,y))))
  φ = ∃x ∀y (P(y,x) ∧ (¬P(x,y) ∨ (Q(x,y) ∨ R(x,y))))

  Applying domain-set on formula: x=1, y=1:
  φ = ∃1 ∀1 (P(1,1) ∧ (¬P(1,1) ∨ (Q(1,1) ∨ R(1,1))))
  φ = ∃1 ∀1 (⊤      ∧ (¬⊤      ∨ (⊥      ∨ ⊥     ))) = ⊥ ... UNSAT

  M ⊭ φ
  ```



--------------------------------------------------------------------------------
### Task 39 [4 Points]
  For each of the formulas of predicate logic below, find a model that
  satisfies the formula and one that does not. Please draw a syntax tree
  and state all free variables while solving this task.
  + (a) `φ = ∀x, ∃y: (P(f(x),y) → Q(f(x),y))`
  + (b) `φ = ∀x,¬∃y: (P(x,z) → (Q(x,y) ∧ Q(y,z)))`


### Solution 39_a
  + Formula: `φ = ∀x, ∃y: (P(f(x),y) → Q(f(x),y))`
  + Satisfying model `Ms` consists of:
    - Domain-set A: `A = {a,b}`
    - Predicate P:  `P^Ms = {(x,y), (x,x)}`
    - Predicate Q:  `Q^Ms = {(x,y), (x,x)}`
    - Function f:   `f^Ms = A → A`
      ```
      Applying De Morgan's Law:
      φ = ∀x, ∃y: ( P(f(x),y) → Q(f(x),y))
      φ = ∀x, ∃y: (¬P(f(x),y) ∨ Q(f(x),y))

      Applying domain-set on formula: x=a, y=a:
      φ = ∀a, ∃a: (¬P(f(a),a) ∨ Q(f(a),a))
      φ = ∀a, ∃a: (¬P(a   ,a) ∨ Q(a   ,a))
      φ = ∀a, ∃a: (¬⊤         ∨ ⊤        ) = ⊤ ... SAT

      Applying domain-set on formula: x=b, y=a:
      φ = ∀b, ∃a: (¬P(f(b),a) ∨ Q(f(b),a))
      φ = ∀b, ∃a: (¬P(b   ,a) ∨ Q(b   ,a))
      φ = ∀b, ∃a: (¬⊥         ∨ ⊥        ) = ⊤ ... SAT

      Both possible combinations evaluate to SAT, therefore:
      Ms ⊨ φ
      ```
  + Falsifying model `Mf` consists of:
    - Domain-set A: `A = {a,b}`
    - Predicate P:  `P^Mf = {(a,a), (a,b), (b,a), (b,b)}`
    - Predicate Q:  `Q^Mf = {(a,a)}`
    - Function f:   `f^Mf = A → A`
      ```
      Applying De Morgan's Law:
      φ = ∀x, ∃y: ( P(f(x),y) → Q(f(x),y))
      φ = ∀x, ∃y: (¬P(f(x),y) ∨ Q(f(x),y))

      Applying domain-set on formula: x=a, y=a:
      φ = ∀a, ∃a: (¬P(f(a),a) ∨ Q(f(a),a))
      φ = ∀a, ∃a: (¬P(a   ,a) ∨ Q(a   ,a))
      φ = ∀a, ∃a: (¬⊤         ∨ ⊤        ) = ⊤ ... SAT

      Applying domain-set on formula: x=b, y=a:
      φ = ∀b, ∃a: (¬P(f(b),a) ∨ Q(f(b),a))
      φ = ∀b, ∃a: (¬P(b   ,a) ∨ Q(b   ,a))
      φ = ∀b, ∃a: (¬⊤         ∨ ⊥        ) = ⊥ ... UNSAT

      Because second combination evaluate to UNSAT:
      Mf ⊭ φ
      ```
  + Syntax Tree:
    ```
    ;              ∀x
    ;               |
    ;              ∃y
    ;               |
    ;               →
    ;           ___/ \___
    ;          /         \
    ;         P           Q
    ;        / \         / \
    ;       /   \       /   \
    ;      f     y     f     y
    ;      |           |
    ;      x           x
    ```
  + Free variables: None, because:
    - `x` has quantifier `∀` and
    - `y` has quantifier `∃`.



### Solution 39_b
  + Formula: `φ = ∀x, ¬∃y: (P(x,z) → (Q(x,y) ∧ Q(y,z)))`
  + Satisfying model `Ms` consists of:
    - Domain-set A:  `A = {a,b,c}`
    - Predicate P:   `P^Ms = {(a,a), (a,b), (b,a), (b,b)}`
    - Predicate Q:   `Q^Ms = {}`
    - Function f:    `f^Ms = A → A`
    - Free variable: `l(z) = a`
      ```
      Applying De Morgan's Law:
      φ = ∀x, ¬∃y: ( P(x,z) → (Q(x,y) ∧ Q(y,z)))
      φ = ∀x, ¬∃y: (¬P(x,z) ∨ (Q(x,y) ∧ Q(y,z)))

      Applying domain-set on formula: x=a, y=a:
      φ = ∀x, ¬∃y: (¬P(x,z) ∨ (Q(x,y) ∧ Q(y,z)))
      φ = ∀a, ¬∃a: (¬P(a,a) ∨ (Q(a,a) ∧ Q(a,a)))
      φ = ∀a, ¬∃a: (¬⊤      ∨ (⊥      ∧ ⊥     )) = ⊥ ... UNSAT

      Applying domain-set on formula: x=b, y=a:
      φ = ∀x, ¬∃y: (¬P(x,z) ∨ (Q(x,y) ∧ Q(y,z)))
      φ = ∀b, ¬∃a: (¬P(b,a) ∨ (Q(b,a) ∧ Q(a,a)))
      φ = ∀b, ¬∃a: (¬⊤      ∨ (⊥      ∧ ⊥     )) = ⊥ ... UNSAT

      Applying domain-set on formula: x=c, y=a:
      φ = ∀x, ¬∃y: (¬P(x,z) ∨ (Q(x,y) ∧ Q(y,z)))
      φ = ∀c, ¬∃a: (¬P(c,a) ∨ (Q(c,a) ∧ Q(a,a)))
      φ = ∀c, ¬∃a: (¬⊤      ∨ (⊥      ∧ ⊥     )) = ⊥ ... UNSAT

      Because there "exists none y" (i.e.: ¬∃y) all possible combinations must
      evaluate to UNSAT, therefore:
      M ⊨ φ
      ```
  + Falsifying model `Mf` consists of:
    - Domain-set A: `A = {a,b}`
    - Predicate P:  `P^Mf = {}`
    - Predicate Q:  `Q^Mf = {}`
    - Function f:   `f^Mf = A → A`
    - Free variable: `l(z) = a`
      ```
      Applying De Morgan's Law:
      φ = ∀x, ¬∃y: ( P(x,z) → (Q(x,y) ∧ Q(y,z)))
      φ = ∀x, ¬∃y: (¬P(x,z) ∨ (Q(x,y) ∧ Q(y,z)))

      Applying domain-set on formula: x=a, y=a:
      φ = ∀x, ¬∃y: (¬P(x,z) ∨ (Q(x,y) ∧ Q(y,z)))
      φ = ∀a, ¬∃a: (¬P(a,a) ∨ (Q(a,a) ∧ Q(a,a)))
      φ = ∀a, ¬∃a: (¬⊥      ∨ (⊥      ∧ ⊥     )) = ⊤ ... SAT

      Because there does "exist one y" (i.e.: ¬∃y is not satisfied)
      Mf does not model φ:
      Mf ⊭ φ
      ```
  + Syntax Tree:
    ```
    ;               ∀x
    ;               |
    ;               ¬
    ;               |
    ;               ∃y
    ;               |
    ;               →
    ;          ____/ \____
    ;         /           \
    ;        P             ∧
    ;       / \         __/ \__
    ;      /   \       /       \
    ;     x    (z)     Q         Q
    ;                / \       / \
    ;               /   \     /   \
    ;              x     y   y    (z)
    ```
  + Free variables:
    - `z`, because there is no quantifier for `z`!



--------------------------------------------------------------------------------
### Task 40 [6 Points]
Consider the following model `M`. The universe `A` is the set of positive
integers including 0. The set of functions `F` is `{0c, 1c, +, ∗}`,
where `0c` and `1c` are functions with arity 0 mapping to 0 and 1.
`+` and `*` are functions taking 2 parameters, where `+` returns the
sum and `*` returns the product of the input, and `*` is prior to `+`.
Finally, let the set of predicates `P` consist of two binary predicates:
`{=,<}`, where `=` is true exactly if both of its inputs are equal and
`<` is true if and only if the first argument is strictly lesser than
the second one. (Note: We will write the functions `+` and `*` as well
as the predicates `=` and `<` in their usual infix notation.)

Translate the following sentences into predicate logic.
Then check whether the given model satisfies the resulting formulas:
+ (a) [1 Point] "For all elements x there is at least one element y,
  such that y is greater than x."
+ (b) [1 Point] "Not for all elements x there exists an element y,
  such that x added to y equals y."

Check whether the given model satisfies the following formulas:
+ (c) [2 Point] `φ = ∃x, ¬∃y: ((x = 0c) ∨ (x = 1c) → (x < y) ∧ (y ∗ y = x))`
+ (d) [2 Point] `φ = ∀x, ∀y: (((x = y) ∧ (x < 1c)) → x ∗ y = x + y)`


### Solution 40
+ Rewritten task for better overview:
  - Universe A: `A = {ℕ ∪ {0}}`
  - Functions F: `F = {0c, 1c, +, *}`
    - Function `0c` has arity 0 (i.e. no parameters) and returns 0.
    - Function `1c` has arity 0 (i.e. no parameters) and returns 1.
    - Function `+` is used as infix notation (i.e. `a+b`) and not in predicate notation (i.e. `+(a,b)`).
      - Function `+` returns the sum.
    - Function `-` is used as infix notation (i.e. `a-b`) and not in predicate notation (i.e. `-(a,b)`).
      - Function `-` returns the difference.
  - Predicates P: `P = {=,<}`
    - Predicate `=` is used as infix notation (i.e. `a=b`) and not in predicate notation (i.e. `=(a,b)`).
      - Predicate `=` returns true iff given variables are equal.
    - Predicate `<` is used as infix notation (i.e. `a<b`) and not in predicate notation (i.e. `<(a,b)`).
      - Predicate `<` returns true iff left variable is lesser than right variable.


+ (a) "For all elements x there is at least one element y,
  such that y is greater than x."
  ```
  φ = ∀x, ∃y: (x < y)
  x=0, y=1:   (0 < 1)   = ⊤ ... SAT
  x=n+1, y=n: (n < n+1) = ⊤ ... SAT
  Indeed for every `x` does exist a `y` which satisfies φ, therefore:
  M ⊨ φ
  ```

+ (b) [1 Point] "Not for all elements x there exists an element y,
  such that x added to y equals y."
  ```
  φ = ¬∀x, ∃y: (x + y = y)
  x=0, y=0:    (0 + 0 = 0) = ⊤ ... SAT
  x=1, y=0:    (1 + 0 = 0) = ⊥ ... UNSAT
  Indeed not for every `x` does exist a `y` which satisfies φ, therefore:
  M ⊨ φ
  ```


+ (c) `φ = ∃x, ¬∃y: (((x = 0c) ∨ (x = 1c)) → ((x < y) ∧ (y ∗ y = x)))`
  ```
  Applying domain-set on formula: x=0, y=0:
  φ = ∃0, ¬∃0: (((0 = 0 ) ∨ (0 = 1 )) → ((0 < 0) ∧ (0 ∗ 0 = 0)))
  φ = ∃0, ¬∃0: ((   ⊤     ∨    ⊥    ) → (   ⊥    ∧        ⊤   ))
  φ = ∃0, ¬∃0: (          ⊤           →          ⊥             ) = ⊥ ... UNSAT

  Applying domain-set on formula: x=0, y=1:
  φ = ∃0, ¬∃1: (((0 = 0 ) ∨ (0 = 1 )) → ((0 < 1) ∧ (1 ∗ 1 = 0)))
  φ = ∃0, ¬∃1: ((   ⊤     ∨    ⊥    ) → (   ⊤    ∧        ⊥   ))
  φ = ∃0, ¬∃1: (          ⊤           →          ⊥             ) = ⊥ ... UNSAT

  Applying domain-set on formula: x=1, y=0:
  φ = ∃1, ¬∃0: (((1 = 0 ) ∨ (1 = 1 )) → ((1 < 0) ∧ (0 ∗ 0 = 1)))
  φ = ∃1, ¬∃0: ((   ⊥     ∨    ⊤    ) → (   ⊥    ∧        ⊥   ))
  φ = ∃1, ¬∃0: (          ⊤           →          ⊥             ) = ⊥ ... UNSAT

  Applying domain-set on formula: x=1, y=1:
  φ = ∃1, ¬∃1: (((1 = 0 ) ∨ (1 = 1 )) → ((1 < 1) ∧ (1 ∗ 1 = 1)))
  φ = ∃1, ¬∃1: ((   ⊥     ∨    ⊤    ) → (   ⊥    ∧        ⊤   ))
  φ = ∃1, ¬∃1: (          ⊤           →          ⊥             ) = ⊥ ... UNSAT

  Because for an element x does no element y exist which lets the formula φ
  evaluate to SAT:
  M ⊨ φ
  ```


+ (d) `φ = ∀x, ∀y: (((x = y) ∧ (x < 1c)) → x ∗ y = x + y)`
  ```
  Applying domain-set on formula: x=0, y=0:
  φ = ∀0, ∀0: (((0 = 0) ∧ (0 < 1 )) → 0 ∗ 0 = 0 + 0)
  φ = ∀0, ∀0: ((   ⊤    ∧    ⊤    ) →       ⊤      ) = ⊤ ... SAT

  Applying domain-set on formula: x=0, y=1:
  φ = ∀0, ∀1: (((0 = 1) ∧ (0 < 1 )) → 0 ∗ 1 = 0 + 1)
  φ = ∀0, ∀1: ((   ⊥    ∧    ⊤    ) →       ⊥      ) = ⊤ ... SAT

  Applying domain-set on formula: x=1, y=0:
  φ = ∀1, ∀0: (((1 = 0) ∧ (1 < 1 )) → 1 ∗ 0 = 1 + 0)
  φ = ∀1, ∀0: ((   ⊥    ∧    ⊥    ) →       ⊥      ) = ⊤ ... SAT

  Applying domain-set on formula: x=1, y=2:
  φ = ∀1, ∀2: (((1 = 2) ∧ (1 < 1 )) → 1 ∗ 2 = 1 + 2)
  φ = ∀1, ∀2: ((   ⊥    ∧    ⊥    ) →       ⊥      ) = ⊤ ... SAT

  Applying domain-set on formula: x=2, y=1:
  φ = ∀2, ∀1: (((2 = 1) ∧ (2 < 1 )) → 2 ∗ 1 = 2 + 1)
  φ = ∀2, ∀1: ((   ⊥    ∧    ⊥    ) →       ⊥      ) = ⊤ ... SAT

  Because φ evaluates to true for all x in combination with all y:
  M ⊨ φ
  ```
