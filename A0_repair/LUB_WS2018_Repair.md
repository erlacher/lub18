# Logic and Computability WS18


## Repair Assignment


--------------------------------------------------------------------------------
### Task_1 [4 Points]
For each of the following sequents, either provide a natural
deduction proof, or a counter-example that proves the sequent invalid.


+ Sequence **a**: `p ∧ q ⊢ p ∨ q`
  - Natural Deduction:
    ```
    1. p ∧ q      | prem
    2. p          | ∧e_1 1
    3. p ∨ q      | ∨i_1 2
    ```


+ Sequence **b**: `p ∨ ¬q ⊢ q → p`
  - Natural Deduction:
    ```
    1. p ∨ ¬q     | prem
    ----------------------------------------( Box1
    2. q          | ass
    ----------------------------------------( Box2
    3. p          | ass
    ----------------------------------------) Box2
    ----------------------------------------( Box3
    4. ¬q         | ass
    5. ⊥          | ¬e 2,4
    6. p          | ⊥e 5
    ----------------------------------------) Box3
    7. p          | ∨e 1,3-3,4-6
    ----------------------------------------) Box1
    8. q → p      | →i 2-8
    ```


+ Sequence **c**: `p → q ⊢ (¬q → p) ∧ (q → ¬p)`
  - Counter Example:
    ```
    Model M: p=1, q=1
    p → q ⊢ (¬q → p) ∧ (q → ¬p)
    1 → 1 ⊢ (¬1 → 1) ∧ (1 → ¬1)
      1   ⊢ ( 0 → 1) ∧ (1 →  0)
      1   ⊢     1    ∧    0
      1   ⊢          0        ... UNSAT
    ```


+ Sequence **d**: `p ∨ (q ∧ r) ⊢ (p ∨ q) ∧ (p ∨ r)`
  - Natural Deduction:
    ```
    01. p ∨ (q ∧ r)   | prem
    ----------------------------------------( Box1
    02. p             | ass
    03. p ∨ q         | ∨i_1 2
    ----------------------------------------) Box1
    ----------------------------------------( Box2
    04. q ∧ r         | ass
    05. q             | ∧e_1 4
    06. p ∨ q         | ∨i_2 5
    ----------------------------------------) Box2
    07. p ∨ q         | ∨e 1,2-3,4-6
    ----------------------------------------( Box3
    08. p             | ass
    09. p ∨ r         | ∨i_1 8
    ----------------------------------------) Box3
    ----------------------------------------( Box4
    10. q ∧ r         | ass
    11. r             | ∧e_2 10
    12. p ∨ r         | ∨i_2 11
    ----------------------------------------) Box4
    13. p ∨ r         | ∨e 1,8-9,10-12
    14. (p ∨ q) ∧ (p ∨ r)   | ∧i 7,13
    ```


+ Sequence **e**: `⊢ p → (q → p)`
  - Natural Deduction:
    ```
    ----------------------------------------( Box1
    1. p            | ass
    ----------------------------------------( Box2
    2. q            | ass
    3. p            | copy 1
    ----------------------------------------) Box2
    4. q → p        | →i 2-3
    ----------------------------------------) Box1
    5. p → (q → p)  | →i 1-4
    ```




--------------------------------------------------------------------------------
### Task_2 [3 Points]
For each of the following sequents, either provide a natural
deduction proof, or a counter-example that proves the sequent invalid.


+ Sequence a: `x = y ⊢ f(y, g(x)) = f(x, g(x))`
  - Natural Deduction:
    ```
    1. x = y                      | premise | t1 = t2
    2. f1(x,f2(x)) = f1(x,f2(x))  | =i: ε ⟹ t = t
    3. f1(y,f2(x)) = f1(x,f2(x))  | =e 1,2: t1 = t2, Φ[t1/x] ⟹ Φ[t2/x]
    ```


+ Sequence b: `∀x¬Q(x) ⊢ ¬∃xQ(x)`
  - Natural Deduction:
    ```
    1. ∀x¬Q(x)    | prem
    ----------------------------------------( Box1
    2. ∃xQ(x)     | ass
    ----------------------------------------( Box2
    3. Q(x0)      | ass | x0 fresh
    4. ¬Q(x0)     | ∀e 1
    5. ⊥          | ¬e 3,4
    ----------------------------------------) Box2
    6. ⊥          | ∃e 2,3-5
    ----------------------------------------) Box1
    7. ¬∃xQ(x)    | ¬i
    ```


+ Sequence c: `∃xQ(x) ∧ ∀yP(y) ⊢ ∃x(Q(x) ∧ P(x))`
  - Counter Example:
    - Model M:
      - Domainset A = {x,y}
      - Predicate P = {y}
      - Predicate Q = {x}
    - Parse Tree:
      ```
      ;                   1 ⊢ 0 ... contradiction
      ;             ______/   \______
      ;            /                 \
      ;          ∧=1                ∃x=0
      ;         /   \                 |
      ;        /     \               ∧=0
      ;     ∃x=1    ∀y=1            /   \
      ;       |       |            /     \
      ;      Q=1     P=1         Q=1     P=0
      ;       |       |           |       |
      ;       x       y           x       x
      ```




--------------------------------------------------------------------------------
### Task_3 [3 Points]
Use the DPLL algorithm (including BCP, learned clauses, and Pure Literals)
to check on paper, if the following CNF formulas are satisfiable.
The variables are chosen in alphabetical order and assignments
start with 0. Write down the steps of the DPLL and draw the conflict
graph. If the formula is satisfiable give a satisfying model, else show
a resolution proof of unsatisfiability.
(Note: You also have to proof all learned clauses!)


```
C1 = {¬a, c}
C2 = {¬a, b, ¬c}
C3 = {¬b, e}
C4 = {a, d}
C5 = {a, ¬c}
C6 = {¬a, ¬e}
C7 = {a, ¬b}
C8 = {b, ¬d}
```


- Boolean Constraint Propagation:
  - Use BCP if there is only 1 Literal in clause: {l}
- Pure Literals:
  - Use PL if BCP is not possible and same literal occurs in several clauses.


| Step         | 1 | 2  | 3     | 4       | 5            | 6 | 7   | 8  | 9   | 10    |
|--------------|---|----|-------|---------|--------------|---|-----|----|-----|-------|
| Decision lvl | 0 | 1  | 1     | 1       | 1            | 0 | 0   | 0  | 0   | 0     |
| Assignment   | - | ¬a | ¬a¬b  | ¬a¬b¬c  | ¬a¬b¬c¬d     | - | a   | ac | abc | abc¬e |
| C1: {¬a,c}   | 1 | ⊤  | ⊤     | ⊤       | ⊤            | 1 | c   | ⊤  | ⊤   | ⊤     |
| C2: {¬a,b,¬c}| 2 | ⊤  | ⊤     | ⊤       | ⊤            | 2 | b¬c | b  | ⊤   | ⊤     |
| C3: {¬b,e}   | 3 | 3  | ⊤     | ⊤       | ⊤            | 3 | 3   | 3  | e   | ⊥{}   |
| C4: {a,d}    | 4 | d  | d     | d       | ⊥{}          | 4 | ⊤   | ⊤  | ⊤   | ⊤     |
| C5: {a,¬c}   | 5 | ¬c | ¬c    | ⊤       | ⊤            | 5 | ⊤   | ⊤  | ⊤   | ⊤     |
| C6: {¬a,¬e}  | 6 | ⊤  | ⊤     | ⊤       | ⊤            | 6 | ¬e  | ¬e | ¬e  | ⊤     |
| C7: {a,¬b}   | 7 | ¬b | ⊤     | ⊤       | ⊤            | 7 | ⊤   | ⊤  | ⊤   | ⊤     |
| C8: {b,¬d}   | 8 | 8  | ¬d    | ¬d      | ⊤            | 8 | 8   | 8  | ⊤   | ⊤     |
| C9: {a}      |   |    |       |         | Learned: {a} | 9 | ⊤   | ⊤  | ⊤   | ⊤     |
| BCP          |   | ¬b | ¬c    | ¬d      |              | a | c   | b  | ¬e  |       |
| Pure Literals|   |    |       |         |              |   |     |    |     |       |
| Decision     |¬a |    |       |         |              |   |     |    |     | UNSAT |


- Resolution Rule: `p ∨ φ, ¬p ∨ ψ ⊢ φ ∨ ψ`


+ Additional to step 5:
  - Conflict Graph 1:
    ![z_conflict_graph_1.png](z_conflict_graph_1.png)
  - Resolution Proof 1:
    ```
    | C4: {a ∨ d}, C8: {b ∨ ¬d}
    |         (a ∨ b), C7: {a ∨ ¬b}
    |                a
    ```


+ Additional to step 10:
  - Conflict Graph 2:
    ![z_conflict_graph_2.png](z_conflict_graph_2.png)
  - Resolution Proof 2:
    ```
    | C3: {¬b ∨ e}, C6: {¬a ∨ ¬e}
    |         (¬b ∨ ¬a), C2: {¬a ∨ b ∨ ¬c}
    |              (¬a ∨ ¬c), C1: {¬a,c}
    |                       ¬a
    ```




--------------------------------------------------------------------------------
### Task_4 [3 Points]
Construct a ROBDD for the given functions and given variable orders:


+ Function a: `f = ¬(a ↔ b) ∧ (c → b) ∧ ¬d ∧ e`
  - i.  `a < b < c < d < e`
    ```
    L00 | f = ¬(a ↔ b) ∧ (c → b) ∧ ¬d ∧ e
    L00 | f = (a ⊕ b) ∧ (c → b) ∧ ¬d ∧ e
    L01 | f_a = (1 ⊕ b) ∧ (c → b) ∧ ¬d ∧ e
    L02 | f_ab = ⊥
    L03 | f_a¬b = (c → 0) ∧ ¬d ∧ e
    L04 | f_a¬bc = ⊥
    L05 | f_a¬b¬c = ¬d ∧ e
    L06 | f_a¬b¬cd = ⊥
    L07 | f_a¬b¬c¬d = e
    L08 | f_a¬b¬c¬de = ⊤
    L09 | f_a¬b¬c¬d¬e = ⊥
    L10 | f_¬a = (0 ⊕ b) ∧ (c → b) ∧ ¬d ∧ e
    L11 | f_¬ab = (c → 1) ∧ ¬d ∧ e
    L12 | f_¬abc = ¬d ∧ e == f_a¬b¬c | Repetition_found
    L13 | f_¬ab¬c = ⊥
    L14 | f_¬a¬b = ⊥
    ```
    ![z_task_4ai.png](z_task_4ai.png)
  - ii. `e < d < a < b < c`
    ```
    L00 | f = ¬(a ↔ b) ∧ (c → b) ∧ ¬d ∧ e
    L00 | f = (a ⊕ b) ∧ (c → b) ∧ ¬d ∧ e

    L01 | f_e = (a ⊕ b) ∧ (c → b) ∧ ¬d
    L02 | f_ed = ⊥
    L03 | f_e¬d = (a ⊕ b) ∧ (c → b)
    L04 | f_e¬da = (1 ⊕ b) ∧ (c → b)
    L05 | f_e¬dab = ⊥
    L06 | f_e¬da¬b = c → 0
    L07 | f_e¬da¬bc = ⊥
    L08 | f_e¬da¬b¬c = ⊤
    L09 | f_e¬d¬a = (0 ⊕ b) ∧ (c → b)
    L10 | f_e¬d¬ab = c → 1
    L11 | f_e¬d¬abc = ⊤
    L12 | f_e¬d¬ab¬c = ⊤
    L13 | f_e¬d¬a¬b = ⊥
    L14 | f_¬e = ⊥
    ```
    ![z_task_4aii.png](z_task_4aii.png)



+ Function b: `f = (a ∧ b) ∨ (a ∧ ¬c ∧ ¬d) ∨ (a ∧ c ∧ d) ∨ (¬a ∧ b ∧ ¬c ∧ d) ∨ (¬a ∧ b ∧ c ∧ ¬d)`
  - i.  `c < a < d < b`
  - [@WolframAlpha](https://www.wolframalpha.com/input/?i=%28a+and+b%29+or+%28a+and+not+c+and+not+d%29+or+%28a+and+c+and+d%29+or+%28not+a+and+b+and+not+c+and+d%29+or+%28not+a+and+b+and+c+and+not+d%29)
    ```
    L00 | f = (a ∧ b) ∨ (a ∧ ¬c ∧ ¬d) ∨ (a ∧ c ∧ d) ∨ (¬a ∧ b ∧ ¬c ∧ d) ∨ (¬a ∧ b ∧ c ∧ ¬d)
    L01 | f_c = (a ∧ b) ∨ (a ∧ d) ∨ (¬a ∧ b ∧ ¬d)
    L02 | f_ca = b ∨ d
    L03 | f_cad = b
    L04 | f_cadb = ⊤
    L05 | f_cad¬b = ⊥
    L06 | f_ca¬d = ⊥
    L07 | f_c¬a = b ∧ ¬d
    L08 | f_c¬ad = ⊥
    L09 | f_c¬a¬d = b == f_cad | Repetition_found

    L10 | f_¬c = (a ∧ b) ∨ (a ∧ ¬d) ∨ (¬a ∧ b ∧ d)
    L11 | f_¬ca = b ∨ ¬d
    L12 | f_¬cad = b == f_cad | Repetition_found
    L13 | f_¬ca¬d = ⊤

    L14 | f_¬c¬a = b ∧ d
    L15 | f_¬c¬ad = b == f_cad | Repetition_found
    L14 | f_¬c¬a¬d = ⊥
    ```
    ![z_task_4bi.png](z_task_4bi.png)
  - ii. `a < c < b < d`
    ```
    L00 | f = (a ∧ b) ∨ (a ∧ ¬c ∧ ¬d) ∨ (a ∧ c ∧ d) ∨ (¬a ∧ b ∧ ¬c ∧ d) ∨ (¬a ∧ b ∧ c ∧ ¬d)
    L00 | f_a = b ∨ (¬c ∧ ¬d) ∨ (c ∧ d)
    L00 | f_ac = b ∨ d
    L00 | f_acb = ⊤
    L00 | f_ac¬b = d
    L00 | f_ac¬bd = ⊤
    L00 | f_ac¬b¬d = ⊥
    L00 | f_a¬c = b ∨ ¬d
    L00 | f_a¬cb = ⊤
    L00 | f_a¬c¬b = ¬d == ¬f_ac¬b | Repetition_found
    L00 | f_¬a = (b ∧ ¬c ∧ d) ∨ (b ∧ c ∧ ¬d)
    L00 | f_¬ac = b ∧ ¬d
    L00 | f_¬acb = ¬d == ¬f_ac¬b | Repetition_found
    L00 | f_¬ac¬b = ⊥
    L00 | f_¬a¬c = b ∧ d
    L00 | f_¬a¬cb = d == f_ac¬b | Repetition_found
    L00 | f_¬a¬c¬b = ⊥
    ```
    ![z_task_4bii.png](z_task_4bii.png)



--------------------------------------------------------------------------------
### Task_5 [2 Points]
Build a Kripke structure from the following symbolically
encoded transition relations and draw the corresponding graph:


  + Kripke_Structure_a:
    ```
    (¬x1 ⊕  x0) ∧ (¬x'1 ↔  x'0) ∨
    ( x1 ∧ ¬x0) ∨
    (¬x1 ∧  x0) ∧ ( x'1 ∧ ¬x'0) ∨
    ( x1 ∧  x0) ∧ (¬x'1 ∧ ¬x'0)
    ```
    - Dissolve individual formulas:
      ```
      Line_1: (( x1 ∧  x0) ∨ (¬x1 ∧ ¬x0)) ∧ (( x'1 ∧ ¬x'0) ∨ (¬x'1 ∧  x'0)) ∨
              (     11      ∨     00    ) ∧ (      10'     ∨       01'    ) ∨

      Line_2: ( x1 ∧ ¬x0) ∨
              (    10   ) ∨

      Line_3: (¬x1 ∧  x0) ∧ ( x'1 ∧ ¬x'0) ∨
              (    01   ) ∧ (     10'   ) ∨

      Line_4: ( x1 ∧  x0) ∧ (¬x'1 ∧ ¬x'0)
              (    11   ) ∧ (     00'   )
      ```
      ![z_task_5a.png](z_task_5a.png)


  + Kripke_Structure_b:
    ```
    (¬x2 ∧ ¬x1 ∧ ¬x0 ∧ (  x'2 ⊕  x'1) ∧ ¬x'0) ∨
    ( x2 ∧ ¬x1 ∧ ¬x0 ∧ (( x'2 ∧ ¬x'1  ∧  x'0) ∨ (¬x'2 ∧ x'1 ∧ x'0)   ∨ (¬x'2 ∧ x'1 ∧ ¬x'0))) ∨
    ( x2 ∧ ¬x1 ∧  x0 ∧ (((x'2 ⊕  x'1) ∧  x'0) ∨ ( x'2 ∧ x'1 ∧ x'0))) ∨
    (¬x2 ∧  x1 ∧  x0 ∧ (         x'1  ⊕  x'0))
    ```
    - Dissolve individual formulas:
      ```
      Line_1: (¬x2 ∧ ¬x1 ∧ ¬x0 ∧ (  x'2 ⊕  x'1) ∧ ¬x'0) ∨
              (¬x2 ∧ ¬x1 ∧ ¬x0 ∧ [( ¬x'2 ∧  x'1 ∧ ¬x'0) ∨ (  x'2 ∧ ¬x'1 ∧ ¬x'0)]) ∨
              (      000       ∧ [         010'         ∨          100'        ]) ∨

      Line_2: ( x2 ∧ ¬x1 ∧ ¬x0 ∧ [( x'2 ∧ ¬x'1  ∧  x'0) ∨ (¬x'2 ∧ x'1 ∧ x'0)   ∨ (¬x'2 ∧ x'1 ∧ ¬x'0)]) ∨
              (      100       ∧ [        101'          ∨         011'         ∨         010'       ])

      Line_3: ( x2 ∧ ¬x1 ∧  x0 ∧ [(( x'2 ⊕  x'1) ∧  x'0) ∨ ( x'2 ∧  x'1 ∧  x'0)]) ∨
              ( x2 ∧ ¬x1 ∧  x0 ∧ [((¬x'2 ∧  x'1  ∧  x'0) ∨ ( x'2 ∧ ¬x'1 ∧  x'0))  ∨ ( x'2 ∧ x'1 ∧ x'0)]) ∨
              (      101       ∧ [(         011'         ∨          101'       )  ∨         111'      ]) ∨

      Line_4: (¬x2 ∧  x1 ∧  x0 ∧ (          x'1  ⊕  x'0))
              (¬x2 ∧  x1 ∧  x0 ∧ ((        ¬x'1  ⊕  x'0) ∨ (         x'1  ⊕ ¬x'0)))
              (      011       ∧ (         #01'          ∨           #10'        ))

      ```
      ![z_task_5b.png](z_task_5b.png)




















--------------------------------------------------------------------------------
