# coding=utf-8
from z3 import *
# [Derek Banas: learn Python in 1h](https://youtu.be/N4mEzFDjqtA)



#-------------------------------------------------------------------------------
u, v, w, x, y, z = Reals('u v w x y z')
# u = Int('u')
# v = Int('v')
# w = Int('w')
# x = Int('x')
# y = Int('y')
# z = Int('z')



print("\n\n\n----------------------------------------")
print("Task_57, Literal_a:")
t57a = Solver()
t57a.add(-18*w - 7*x + 3*y + 2*z == -137)
t57a.add(7*w - 18*x + 15*y + 20*z == 410)
t57a.add(17*w + x - 20*y - 5*z == 389)
t57a.add(-2*w + 2*x + 2*y + 6*z == -50)
print(t57a.check())
print(t57a.model())



print("\n\n\n----------------------------------------")
print("Task_57, Literal_b:")
t57b = Solver()
t57b.add(-9*w - 19*x + 4*y - 8*z == -152)
t57b.add(-15*w + 10*x + 20*y + 14*z == -115)
t57b.add(2*w - 20*x + 20*y - 12*z == 86)
t57b.add(-15*w + 18*x + 11*y - 10*z == 0)
print(t57b.check())
print(t57b.model())



print("\n\n\n----------------------------------------")
print("Task_57, Literal_c:")
t57c = Solver()
t57c.add(3*u - 16*v + 6*w + 5*x + 11*y - 10*z == 91)
t57c.add(-10*u + 14*v - 12*w + 19*x + 13*y + 6*z == 379)
t57c.add(-4*u - 16*v - 4*x - 16*y - 16*z == -100)
t57c.add(5*u - 7*v - 12*w + 13*x - y - 5*z == 124)
t57c.add(4*u - 10*v + 8*w - 8*x + 19*y + 4*z == 252)
print(t57c.check())
print(t57c.model())



#-------------------------------------------------------------------------------
alice = z3.Int('alice')
bob = z3.Int('bob')
carol = z3.Int('carol')



print("\n\n\n----------------------------------------")
print("Task_58, Literal_a:")
t58a = Solver()
t58a.add(alice * bob * carol == 72)
t58a.add(alice + bob + carol == 14)
t58a.add(alice <= bob)
t58a.add(bob <= carol)
print(t58a.check())
print(t58a.model())



print("\n\n\n----------------------------------------")
print("Task_58, Literal_b:")
t58b = Solver()
t58b.add(alice * bob * carol == 72)
t58b.add(alice + bob + carol == 14)
t58b.add(alice < bob)   # Additional constraint 1
t58b.add(bob <= carol)
print(t58b.check())
print(t58b.model())



print("\n\n\n----------------------------------------")
print("Task_58, Literal_c:")
t58c = Solver()
t58c.add(alice * bob * carol == 72)
t58c.add(alice + bob + carol == 14)
t58c.add(alice < bob)   # Additional constraint 1
t58c.add(bob < carol)   # Additional constraint 2
print(t58c.check())
if t58c.check() == sat:
    print(t58c.model())
else:
    print("No model available!")




# int add(int a, int b)
# {
#   int c = 0;
#   if (a > 0 && b > 0)
#     c = a + b;
#   assert(c >= 0); // if(c < 0) exit();
#   return c;
# }
print("\n\n\n----------------------------------------")
print("Task_59:")
a = z3.Int('a')
b = z3.Int('b')
c = z3.Int('c')
t59 = Solver()
t59.add(If(And((a > 0),(b > 0)),(c == a + b),(c == 0)))
t59.add(c < 0)
if t59.check() == sat:
    print(t59.model())
else:
    print("No model available!")
