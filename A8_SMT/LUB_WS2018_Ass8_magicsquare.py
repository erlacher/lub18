from z3 import *

MagicSquare = [[z3.Int("MagicField_%s_%s" % (x, y)) for x in range(4)] for y in range(4)]
MagicNumber = z3.Int('MagicNumber')
s = Solver()

# number restriction
number_c = [And(MagicSquare[x][y]>0,MagicSquare[x][y]<MagicNumber) for x in range(4) for y in range(4) ]
s.add(number_c)

# sum of row equals MagicNumber
for x in range(4):
    row_sum = z3.Int('row_sum_%s'%(x))
    row_sum = 0
    for y in range(4):
        row_sum +=  MagicSquare[x][y]
    s.add(row_sum == MagicNumber)

# sum of column equals MagicNumber
for y in range(4):
    column_sum = z3.Int('column_sum_%s'%(y))
    column_sum = 0
    for x in range(4):
        column_sum += MagicSquare[x][y]
    s.add(column_sum == MagicNumber)

# diagonal 1
diagonal_1 = z3.Int('diagonal_1')
diagonal_1 = MagicSquare[0][0] + MagicSquare[1][1] + MagicSquare[2][2] + MagicSquare[3][3]
s.add(diagonal_1 == MagicNumber)

# diagonal 2
diagonal_2 = z3.Int('diagonal_2')
diagonal_2 = MagicSquare[0][3] + MagicSquare[1][2] + MagicSquare[2][1] + MagicSquare[3][0]
s.add(diagonal_2 == MagicNumber)

# Distinct
distinct_c = [z3.Distinct([MagicSquare[x][y] for x in range(4) for y in range(4) ])]
s.add(distinct_c)

if(s.check()==sat):
    m = s.model()
    print('------------------')
    print('\n'.join(['  '.join(['%2s' % m.evaluate(MagicSquare[x][y]) for x in range(4) ]) for y in range(4) ]))
    print('------------------')
    print('row_sum: %s' % m.evaluate(row_sum))
    print('column_sum: %s' % m.evaluate(row_sum))
    print('diagonal_1: %s' % m.evaluate(diagonal_1))
    print('diagonal_2: %s' % m.evaluate(diagonal_2))
    print('------------------')
    print('MagicNumber: %s' % m.evaluate(MagicNumber))
